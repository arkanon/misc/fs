#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2019/12/31 (Tue) 12:20:47 -03
# 2019/04/18 (Thu) 20:42:29 -03

  export DISPLAY=:0
  . /etc/default/keyboard;
  setxkbmap $XKBLAYOUT -option "$XKBOPTIONS" -model "$XKBMODEL" -variant "$XKBVARIANT";
  /fs/bin/synergyc -n synergyc synergys

# EOF
