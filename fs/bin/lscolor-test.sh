#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/09/09 (Wed) 16:19:36 -03

# What do the different colors mean in ls? <http://askubuntu.com/a/17300>


  typeset -A arr

  arr=(
        [bd]="block device"
        [ca]="capability"
        [cd]="character device"
        [di]="directory"
        [do]="door"
        [ec]="endcode"
        [ex]="executable"
        [fi]="normal file"
        [lc]="leftcode"
        [ln]="symbolic link"
        [mh]="multihardlink"
        [mi]="missing file"
        [no]="global default"
        [or]="orphan symlink"
        [ow]="other writable"
        [pi]="named pipe"
        [rc]="rightcode"
        [rs]="reset"
        [sg]="set gid"
        [so]="socket"
        [st]="sticky"
        [su]="set uid"
        [tw]="sticky other writable"
      )

  (
    IFS=:
    for i in $LS_COLORS
    do
      key=${i%=*}
      [[ ${arr[$key]} ]] && samp=${arr[$key]} || samp=$key
      echo -e "\e[${i#*=}m$samp\e[m"
    done
  )

