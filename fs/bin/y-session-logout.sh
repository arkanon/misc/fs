#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/08/14 (Fri) 16:10:37 -03
# 2020/05/28 (Thu) 14:36:13 -03

# grub=/export/grub
# grub=/boot/efi/grub
#
# grub-editenv $grub/grubenv create
# grub-set-default --boot-directory=${grub%/*} " XUbuntu 20.04 vol /root/@1"
# grub-editenv $grub/grubenv list


     script=$(readlink -fm "$0")
       lock=/tmp/${script##*/}
  grub_boot="/export"
   grub_win=" Windows 10"

  [[ -L $lock ]] && { echo "O script já está em excução."; exit 1; }
  ln -fs $BASHPID $lock
  trap "rm $lock; exit" INT EXIT

      ipref="/fs/share/icon/actions/system"

       lock=( "Bloquear sessão"       $ipref-lock_screen.svg   1 )
     logout=( "Encerrar sessão"       $ipref-logout.svg        1 )
     switch=( "Alternar usuário"      $ipref-switch_user.svg   1 )
     reboot=( "Reiniciar"             $ipref-reboot.svg        1 )
        win=( "Reiniciar no Windows"  $ipref-reboot_win.svg    1 )
       halt=( "Desligar"              $ipref-halt.svg          1 )
    suspend=( "Suspender"             $ipref-suspend.svg       1 )
  hibernate=( "Hibernar"              $ipref-hibernate.svg     1 )
     hybrid=( "Suspensão híbrida"     $ipref-hybrid_sleep.svg  1 )

  yad \
    --skip-taskbar   \
    --undecorated    \
    --sticky         \
    --center         \
    --on-top         \
    --form           \
    --text-align     "center" \
    --text           "<span size=\"x-large\">Escolha como sair da sessão</span>" \
    --borders        10   \
    --columns        3    \
  \
    --field          ":LBL" "" \
    --field          "!${lock[     1]}!${lock[     0]}:BTN"   "bash -c ' xfce4-screensaver-command -l           '" \
    --field          "!${reboot[   1]}!${reboot[   0]}:BTN"   "bash -c ' xfce4-session-logout -f --reboot       '" \
    --field          "!${suspend[  1]}!${suspend[  0]}:BTN"   "bash -c ' xfce4-session-logout -f --suspend      '" \
  \
    --field          ":LBL" "" \
    --field          "!${logout[   1]}!${logout[   0]}:BTN"   "bash -c ' xfce4-session-logout -f --logout       '" \
    --field          "!${win[      1]}!${win[      0]}:BTN"   "bash -c ' grub-reboot --boot-directory=$grub_boot \"$grub_win\" ; xfce4-session-logout -f --reboot '" \
    --field          "!${hibernate[1]}!${hibernate[0]}:BTN"   "bash -c ' xfce4-session-logout -f --hibernate    '" \
  \
    --field          ":LBL" "" \
    --field          "!${switch[   1]}!${switch[   0]}:BTN"   "bash -c ' xfce4-session-logout -f --switch-user  '" \
    --field          "!${halt[     1]}!${halt[     0]}:BTN"   "bash -c ' xfce4-session-logout -f --halt         '" \
    --field          "!${hybrid[   1]}!${hybrid[   0]}:BTN"   "bash -c ' xfce4-session-logout -f --hybrid-sleep '" \
  \
    --button         "Cancelar:1" \

# ; xfce4-session-logout -f --reboot'" \


# EOF
