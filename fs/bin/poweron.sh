# /fs/bin/poweron.sh

# Arkanon <arkanon@lsd.org.br>
# 2020/09/11 (Fri) 20:33:09 -03
# 2020/07/19 (Sun) 21:04:16 -03

   now=$(LC_ALL=C date +"%Y/%m/%d %a %H:%M:%S")
  host=${HOSTNAME%%.*}
    to=poweron@lsd.org.br

  . $ADM_DIR/.s-m

  conf="
         from          root@$host
         logfile       /fs/var/log/poweron-$host
         host          smtp.gmail.com
         auth
         tls
         tls_certcheck off
         port          587
         user          $user
         password      $pass
       "

  mail0() { echo -e "Subject: $2\n\n$3" | msmtp -C <(echo "$conf") $1 ; }

  mail0 $to "$host ligada em $now" ""

# EOF
