#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/07/29 (Wed) 11:59:27 -03

# apt|yum install source-highlight
# dpkg -L  libsource-highlight-common | grep lesspipe
# rpm  -ql    source-highlight        | grep lesspipe
# source-highlight --lang-list

  for file in "$@"
  {
    shl="source-highlight --failsafe -f esc --style-file=esc.style"
    case $file in
    *.tar|*.tgz|*.gz|*.bz2|*.xz ) lesspipe "$file"                           ;;
    *.bash*|*.profile           ) $shl -i  "$file" --lang-def=sh.lang        ;;
    *Makefile|*makefile         ) $shl -i  "$file" --lang-def=makefile.lang  ;;
    *ChangeLog|*changelog       ) $shl -i  "$file" --lang-def=changelog.lang ;;
    *                           ) $shl -i  "$file" --infer-lang
    esac
  }

# EOF
