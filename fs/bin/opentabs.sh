#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/08/27 (Thu) 13:59:03 -03
# 2020/08/21 (Fri) 14:39:56 -03
# 2020/07/21 (Tue) 10:38:35 -03
# 2020/06/15 (Mon) 15:10:47 -03
# 2020/05/11 (Mon) 18:34:17 -03
# 2020/04/27 (Mon) 11:25:06 -03
# 2020/03/17 (Tue) 20:33:30 -03
# 2019/11/21 (Thu) 09:07:31 -03


# terminal=lxterminal
  terminal=xfce4-terminal

  typeset -A tabs1 tabs2

         new_tab=Control_L+Shift_L+T
        tab_left=Control_L+Page_Up
       tab_right=Control_L+Page_Down
    clear_screen=Control_L+L
   close_session=Control_L+D
       first_tab=Alt_L+1
        goto_tab=Alt_L
           enter=Return
     with_screen=Return
  without_screen=c

  tabs1=(
        # [tty]='screen|command|enter'
          [01]='s|cd ~/git/ansible'
          [03]='s|cd ~/git/ansible/roles/vpn-router/templates'
          [05]='s|cd ~/git/ansible'
        # [06]='x'
          [07]='s|. ~/git/ansible/monit.sh|false'
          [09]='s'
          [11]='s'
          [13]='s'
          [15]='s'
          [17]='s'
          [19]='s'
          [20]='n'
          [21]='n'
          [22]='n'
          [23]='n'
        )

  tabs2=(
          [25]='s|vpn a|false'
          [27]='s|ping 8.8.8.8'
        )



  {

    win1=$(xdotool getactivewindow)
    xdotool="xdotool windowactivate $win1"
    n=1
    for i in $(xargs -n1 <<< ${!tabs1[*]} | sort)
    {
      data=${tabs1[$i]}
      IFS='|' eval 'data=($data)'
      screen=$([[ ${data[0]} == s ]] && echo $with_screen || echo $without_screen)
      action=${data[1]}
         run=${data[2]}
      (( n > 1 )) && \
      {
        $xdotool key $new_tab
        $xdotool key $screen
        while [[ $(xdotool getwindowname $win1) == LXTerminal ]]; do sleep .1; done
      }
      $xdotool key $clear_screen
      if [[ $action ]]
      then
        $xdotool type "$action"
        $run && $xdotool key $enter
        [[ $action =~ ^cd\  ]] && { $xdotool key $clear_screen ; }
      fi
      sleep .5
      ((n++))
    }

    preexistent=$(wmctrl -lx | grep $terminal | cut -d\  -f1)

    $terminal
    sleep .1

    win2=$(wmctrl -lx | grep $terminal | cut -d\  -f1 | grep -vf <(echo "$preexistent"))
    xdotool="xdotool windowactivate $win2"

    wmctrl -ir $win2 -b remove,maximized_vert,maximized_horz
    xdotool windowsize --usehints $win2 100 20

    n=1
    for i in $(xargs -n1 <<< ${!tabs2[*]} | sort)
    {
      data=${tabs2[$i]}
      IFS='|' eval 'data=($data)'
      screen=$([[ ${data[0]} == s ]] && echo $with_screen || echo $without_screen)
      action=${data[1]}
         run=${data[2]}
      (( n > 1 )) && $xdotool key $new_tab
      $xdotool key $screen
      while [[ $(xdotool getwindowname $win2) == LXTerminal ]]; do sleep .1; done
      $xdotool key $clear_screen
      if [[ $action ]]
      then
        $xdotool type "$action"
        $run && $xdotool key $enter
        [[ $action =~ ^cd\  ]] && { $xdotool key $clear_screen ; }
      fi
      sleep .1
      ((n++))
    }

    xdotool="xdotool windowactivate $win1"
    $xdotool key $first_tab
    for i in $(xargs -n1 <<< ${!tabs1[*]} | sort)
    {
      data=${tabs1[$i]}
      do=$([[ $data == x ]] && echo $close_session || echo $tab_right)
      $xdotool key $do
    }

  } &



# EOF



rem()
{

  typeset -A actions
  actions=( [k]=key [t]=type )

  monit()
  {
    # abre uma aba sem screen, que ficará com tty par e será fechada ao final do processo)
    run .2 k $new_tab
    run .1 k $without_screen
    # abre outra aba sem screen, esta ficará com tty impar e será mantida
    run .2 k $new_tab
    run .1 k $without_screen
    # se está na vpn dá um ssh, senão apenas limpa a tela
    if host softl030-250 &> /dev/null
    then
      run .1 t ssh softl030-250
      run .1 k $enter
      run .1 k $with_screen
      run .1 t cd \~ \; . git/ansible/monit.sh
      run .1 k $enter
    else
      run .1 k $clear_screen
    fi
  }

  run()
  {
    local wait action
      wait=$1; shift
    action=$1; shift
    sleep $wait ; xdotool ${actions[$action]} "$*"
  }

}

