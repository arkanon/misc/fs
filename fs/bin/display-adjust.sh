#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/07/07 (Tue) 03:09:18 -03
# 2019/05/20 (Mon) 09:10:48 -03
# 2018/10/25 (Thu) 09:41:59 -03
# 2018/05/18 (Fri) 12:43:40 -03
# 2018/01/16 (Tue) 15:11:00 -02
# 2018/01/15 (Mon) 14:00:56 -02
# 2018/01/08 (Mon) 10:35:00 -02
# 2017/12/01 (Fri) 17:06:12 -02
# 2017/11/29 (Wed) 13:34:38 -02
# 2017/04/03 (Mon) 08:39:30 BRS
# 2017/03/30 (Thu) 17:03:26 BRS
# 2017/02/23 (Thu) 08:43:09 BRS
# 2017/01/04 (Qua) 08:35:13 BRD
# 2016/11/19 (Sat) 19:01:18 BRD
# 2016/11/16 (Qua) 13:25:22 BRD
# 2016/08/29 (Seg) 08:47:01 BRS
# 2015/09/16 (Wed) 13:09:31 BRS

# <http://unix.stackexchange.com/questions/227876/how-to-set-custom-resolution-using-xrandr-when-the-resolution-is-not-available-i>
# <http://askubuntu.com/questions/136139/xrandr-configure-crtc-0-failed-when-trying-to-change-resolution-on-external-m>
# Making Resolution Permanent via XRandR <http://bbs.archlinux.org/viewtopic.php?pid=893296#p893296>
# Configuração de Monitores <http://poligraph.sharepoint.com/sites/UNGP-MIB/_layouts/15/WopiFrame.aspx?sourcedoc={465f9c0b-2a43-4b08-ad91-ef2092712c5f}&action=view&wd=target(Linux.one|a7f2c222-ad53-4b0c-9edf-c1d8ca63d654/Configuração+de+monitores/7b56f02b-6bd4-46ff-99f1-cc9e2fdb6ed9%29>

# exit

  echo=${1:-eval} # run [$0 echo] for debug

  add_screen()
  {
        i=${i:-1}           # screen index (1..)
    w[$i]=$((0+10#$1))      # width    (px)
    h[$i]=$((0+10#$2))      # height   (px)
    f[$i]=$3                # refresh rate (Hz)
    p[$i]=$4                # position (px)
    r[$i]=$5                # rotation (normal, right, left)
    s[$i]=$6                # screen name (DP1, VGA2, HDMI1, ...)
  # s[$i]=$( xrandr | grep -oP "^[^ ]+(?= .*(${w[$i]}x${h[$i]}|${h[$i]}x${w[$i]})+)" )
  # s[$i]=$(
  #          xrandr \
  #          | grep -E '^[^ ]|[ *]\+( |$)' \
  #          | tr '\n' '$' \
  #          | sed -r 's/\$   / /g' \
  #          | tr '$' '\n' \
  #          | grep -oP "^[^ ]+(?= .* (${w[$i]}x${h[$i]}|${h[$i]}x${w[$i]}))"
  #        )
    echo "${s[$i]}"
    m[$i]=${w[$i]}x${h[$i]} # mode
    l[$i]=$(gtf ${w[$i]} ${h[$i]} ${f[$i]} | grep -i modeline | cut -d\  -f5-) # modeline
    o[$i]="--output  ${s[$i]} --mode ${m[$i]} --pos ${p[$i]} --rotate ${r[$i]}"
    $echo xrandr --newmode               \"${m[$i]}\"     ${l[$i]} 2> /dev/null
  # echo "new $? $(date +'%Y/%m/%d %a %H:%M:%S')" >> /tmp/xrandr.log
    $echo xrandr --addmode ${s[$i]}        ${m[$i]}                2> /dev/null
  # echo "add $? $(date +'%Y/%m/%d %a %H:%M:%S')" >> /tmp/xrandr.log
    $echo
    ((i++))
  }

  case ${HOSTNAME%%.*} in

    softl070-008 )
      add_screen   1280   1024   60.00   $((0              ))x0   right    VGA-1
      add_screen   1920   1080   60.00   $((${h[1]}        ))x0   normal   HDMI-2
      add_screen   1600    900   60.00   $((${h[1]}+${w[2]}))x0   left     DP-1
      ;;

    softl030-250 | HOST )
      add_screen   1920   1080   75.00   $((0              ))x0   normal   DP1
      add_screen   1600    900   60.00   $((${w[1]}        ))x0   left     DP2
      ;;

  # softl030-250 )
  #   add_screen   1280   1024   60.00   $((0              ))x0   right    DVI-1
  #   add_screen   1920   1080   60.00   $((${h[1]}        ))x0   normal   DP-1
  #   add_screen   1600    900   60.00   $((${h[1]}+${w[2]}))x0   left     DP-2
  #   ;;

    softl070-008-vm02 )
      add_screen   1024    768   60.00   $((0              ))x0   normal   Virtual-1
      ;;

    deadpool )
      # --output VGA-0   --mode 1920x1080 --pos 0x350  --rotate normal --primary \
      xrandr \
        --output VGA-0   --mode 1920x1080 --pos    0x0 --rotate normal --primary \
        --output DVI-I-1 --mode 1600x900  --pos 1920x0 --rotate normal \
        --output DVI-I-0 --off \
        --output HDMI-0  --off
      ;;

    * )
      echo "Defina o layout de displays em $0"

  esac

  $echo xrandr   ${o[1]}   ${o[2]}   ${o[3]}
# echo "set $? $(date +'%Y/%m/%d %a %H:%M:%S')" >> /tmp/xrandr.log

# [ "$echo" ] && { echo; xrandr; }

# EOF
