#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2020/04/06 (Mon) 13:22:11 -03

# <http://github.com/symless/synergy-core>
# <http://github.com/symless/synergy-core/releases>
# <http://github.com/symless/synergy-core/wiki/Compiling>



  server=deadpool
  client=softl030-043

  ver=1.11.1

  time wget --content-disposition https://github.com/symless/synergy-core/archive/v$ver-stable.tar.gz
  tar zxf synergy-core-$ver-stable.tar.gz
  cd synergy-core-$ver-stable/
  mkdir -p build/
  cd       build/
  time sudo apt install qtcreator qtbase5-dev qttools5-dev cmake make g++ xorg-dev libssl-dev libx11-dev libsodium-dev libgl1-mesa-glx libegl1-mesa libcurl4-openssl-dev libavahi-compat-libdnssd-dev qtdeclarative5-dev libqt5svg5-dev libsystemd-dev
  time cmake .. # 0m32,469s
  time  make    # 4m23,936s
  cp -a bin/ bin-stripped/
  strip      bin-stripped/*



  sudo cp  -a  bin-stripped/         /export/synergy/
       scp -pr bin-stripped/ $client:/export/synergy/



  mkdir -p /fs/etc/
  mkdir -p /fs/bin/
  mkdir -p /fs/share/lightdm/lightdm.conf.d/



  cat << EOT >| /fs/share/lightdm/lightdm.conf.d/999-synergys.conf
[Seat:*]
#display-setup-script=/usr/bin/synergys --name synergys --no-tray --enable-drag-drop -c /fs/etc/synergy.conf
greeter-setup-script=/usr/bin/synergys --name synergys --no-tray --enable-drag-drop -c /fs/etc/synergy.conf
EOT



  cat << EOT >| /fs/share/lightdm/lightdm.conf.d/999-synergyc.conf
[Seat:*]
#display-setup-script=/fs/bin/synergyc.sh
greeter-setup-script=/fs/bin/synergyc.sh
EOT



  cat << EOT >| /fs/bin/synergyc.sh
#!/bin/bash
  export DISPLAY=:0
  . /etc/default/keyboard;
  setxkbmap $XKBLAYOUT -option "$XKBOPTIONS" -model "$XKBMODEL" -variant "$XKBVARIANT";
  /fs/bin/synergyc synergys
EOT



  cat << EOT >> /fs/etc/hosts
  10.1.1.2  $server  synergys
  10.1.1.3  $client  synergyc
EOT



  sudo mkdir /var/fs/
  sudo chown arkanon:arkanon /var/fs/

       ln -s   /fs/share/lightdm/lightdm.conf.d/999-synergys.conf /var/fs/150-synergy.conf # $server
       ln -s   /fs/share/lightdm/lightdm.conf.d/999-synergyc.conf /var/fs/150-synergy.conf # $client
       ln -fs  /var/fs/150-synergy.conf                           /fs/share/lightdm/lightdm.conf.d/150-synergy.conf

       ln -fs  /export/synergy/synergy{s,c}                       /fs/bin/

       ln -nfs /usr/share/lightdm/lightdm.conf.d                  /fs/share/lightdm/lightdm.conf.d/.d
  sudo ln -fs  /fs/share/lightdm/lightdm.conf.d/150-synergy.conf  /usr/share/lightdm/lightdm.conf.d/

  sudo mv      /etc/hosts                                         /etc/hosts-
  sudo ln -s   /fs/etc/hosts                                      /etc/hosts



# EOF
