# ~/.profile

# Arkanon <arkanon@lsd.org.br>
# 2024/09/19 (Thu) 10:58:56 -03



  main()
  {

  # declare -rx TMOUT=30

  # export      PASH_TOP=/media/backup-export/src/git/github.com/pash
    export          PATH=/fs/sbin:$PATH:~/Android/Sdk/emulator:$PASH_TOP
  # export XDG_DATA_DIRS=$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share
  # export GTK_IM_MODULE=cedilla

    export adm_dir=$(< /fs/home/.adm)
    export    ad_c=$adm_dir/.

    [[ -f $ad_c ]] && . $ad_c
    export username
    export sudo_s=$adm_dir/.$([[ $default_gw =~ ^172.(17|23) ]] && echo a || echo l)
    [[ -f $sudo_s-${HOSTNAME%%.*} ]] && sudo_s=$sudo_s-${HOSTNAME%%.*}
    ssh_keys(){ ssh_keys=(
      $adm_dir/.pphrase1:$HOME/.ssh/privkey1
      $adm_dir/.pphrase2:$HOME/.ssh/privkey2
    ); }; export -f ssh_keys; ssh_keys

    (( UID != 0 )) && creds

    export VIMSIGN1="Arkanon <arkanon@lsd.org.br>" # F3
    export VIMSIGN2="Arkanon <$username@$domain>"  # F4

  }



  mods()
  {
    creds -k
    creds
    restart=false
    for i in /media/backup-media /opt/firefox
    {
      df $i | grep -q $i || { mount $i; restart=true; }
    }
    if $restart
    then
      sleep .5
      xfce4-panel -q
      sleep .5
      { nohup xfce4-panel > /dev/null & };
    # sudo modprobe garmin_gps
      for i in modules sinks sources clients cards samples; { pacmd list-$i; } | grep -q 'ALC3204 Analog' || pacmd load-module module-alsa-sink
      sudo cp -a {/export/fs-repo/copy/soft005258,}/var/lib/alsa/asound.state
      sudo alsactl restore
      sudo modprobe v4l2loopback devices=1 exclusive_caps=1 video_nr=2 card_label=fake-cam
    fi
  }



  vimcf()
  {
    echo 'echo -e "\nhi Comment ctermfg=lightblue\nhi String  ctermfg=lightred" >> /etc/vimrc'
  }



  main



# EOF
