# $env:USERPROFILE\Documents\PowerShell\profile.ps1
# $env:HOME/.config/powershell/profile.ps1

# Arkanon <arkanon@lsd.org.br>
# 2020/09/12 (Sat) 00:17:01 -03
# 2020/07/02 (Thu) 23:16:19 -03
# 2020/05/25 (Mon) 09:02:19 -03
# 2020/05/22 (Fri) 18:26:46 -03
# 2020/05/21 (Thu) 21:05:09 -03
# 2020/05/20 (Wed) 18:53:47 -03
# 2019/09/08 (Sun) 16:06:31 -03



  # PowerShell: Is there an automatic variable for the last execution result? <http://stackoverflow.com/a/52485269>
  $PSDefaultParameterValues['Out-Default:OutVariable'] = '__'



  if ($IsWindows)
  {
    $env:path += ";$env:ProgramFiles/Vim/vim82"
    $env:path += ";$env:ProgramFiles/7-Zip"
    $env:path += ";$env:UserProfile/vimfiles/ctags\5.8"
  }



  function l()
  {
    $psv = ( wsl -l -v | Select-String '\*' ) -split ' ' | select -last 1
    if ( $psv -eq 1 ) { l_wsl1 @args } else { l_wsl2 @args }
  }



  function l_wsl2()
  {

    $ls = ( $profile | Split-Path ) + "\ls.sh"
    $ls = bash -c "wslpath '$ls'"

    if ($IsWindows)
    {
      if ( get-command -errorAction SilentlyContinue wsl )
      {
        $ar = $args -join "' '"
        iex "bash $ls '$ar'"
      }
      else
      {
        "wsl não instalado"
      }
    }
    else
    {
      bash $ls $args
    }

  }



  function l_wsl1()
  {

    $bs = "\'1"
    $lc = "LC_ALL= LC_NUMERIC=pt_BR.UTF-8 BLOCK_SIZE=$bs"
    $ls = "ls -lapisbvT0 --color=always --time-style='+%Y/%m/%d %a %T %:::z' --group-directories-first"
    if ($IsWindows)
    {
      if ( get-command -errorAction SilentlyContinue wsl )
      {

        bash -c (
                  ' eval ' `
                + $( $lc -replace "\\" , "\\\\\" ) `
                + ' ' `
                + $( $ls -replace "\'" , "\'" ) `
                + ' \$( for i in \"' `
                + $( $args -replace '\\$' -replace '\\', '\\' -join '\" \"' ) `
                + '\"; { echo \"\\\"\$( wslpath -u \"\$i\" | sed \"s/\*/\\\"*\\\"/g\" )\\\"\" ; } ) '
                )

      }
      else
      {
        "wsl não instalado"
      }
    }
    else
    {
      bash -c "$lc $ls $args"
    }

  }


  function time
  {
      $command = get-history -count 1
    ( $command.endExecutionTime - $command.startExecutionTime ).totalSeconds
  }

  function lc { invoke-expression $(get-history -count 1) }



  $smtp = `
  @{

     ms = `
     @{
        port             = "25"
      # enableSSL        = ""
      # passwordAsSecure = ""
      # passwordFromFile = ""
      }

     aws = `
     @{
        port             = "587"
        enableSSL        = "tls"
      # passwordAsSecure = ""
      # passwordFromFile = ""
      }

     gmail = ` # <http://myaccount.google.com/lesssecureapps>
     @{
        port             = "587"
        enableSSL        = "tls"
      # passwordAsSecure = ""
      # passwordFromFile = ""
      }

   }

   . $env:ADM_DIR/.smtp.ps1



# EOF
