#!/bin/bash

  export LC_ALL=
  export LC_NUMERIC=pt_BR.UTF-8
  export BLOCK_SIZE="'1"

  for (( i=1; i<=$#; i++ ))
  {
    list[$i]=$( [[ ${!i} =~ -.* ]] && echo ${!i} || echo "'$( wslpath "${!i}" | sed "s/[*?]/'&'/g" )'" )
  }

  eval "ls -lapisbvT0 --color=always --time-style='+%Y/%m/%d %a %T %:::z' --group-directories-first ${list[@]}"



# EOF
