" man.vim

" export                 MANPAGER="/usr/bin/most -s"
"
" export                    PAGER="bash -c \"
"                                            col -b -x | vim -R \
"                                                            -c 'set ft=man nomod nolist' \
"                                                            -c ' map q       :q<CR>    ' \
"                                                            -c ' map <SPACE> <C-D>     ' \
"                                                            -c ' map b       <C-U>     ' \
"                                                            -c 'nmap K       :Man <C-R>=expand(\"<cword>\")<CR><CR>' \
"                                                            -
"                                          \" " # <http://vim.wikia.com/wiki/Using_vim_as_a_man-page_viewer_under_Unix>

  set ft=man nomod nolist

   map q       :q<CR>
   map <SPACE> <C-D>
   map b       <C-U>
  nmap K       :Man <C-R>=expand("<cword>")<CR><CR>

  " DrChip's additional man.vim stuff
  syn match   manSectionHeading  "^\s\+[0-9]\+\.[0-9.]*\s\+[A-Z].*$"     contains=manSectionNumber
  syn match   manSectionNumber   "^\s\+[0-9]\+\.[0-9]*"                  contained
  syn region  manDQString        start='[^a-zA-Z"]"[^", )]'lc=1 end='"'  contains=manSQString
  syn region  manSQString        start="[ \t]'[^', )]"lc=1      end="'"
  syn region  manSQString        start="^'[^', )]"lc=1          end="'"
  syn region  manBQString        start="[^a-zA-Z`]`[^`, )]"lc=1 end="[`']"
  syn region  manBQSQString      start="``[^),']"               end="''"
  syn match   manBulletZone      transparent "^\s\+o\s"                  contains=manBullet
  syn case match
  syn keyword manBullet                                                  contained o
  syn match   manBullet                                                  contained "\[+*]"
  syn match   manSubSectionStart "^\*" skipwhite nextgroup=manSubSection
  syn match   manSubSection      ".*$"                                   contained

  hi link manSectionNumber       Number
  hi link manDQString            String
  hi link manSQString            String
  hi link manBQString            String
  hi link manBQSQString          String
  hi link manBullet              Special
  hi      manSubSectionStart     term=NONE      cterm=NONE      gui=NONE      ctermfg=black ctermbg=black guifg=navyblue guibg=navyblue
  hi      manSubSection          term=underline cterm=underline gui=underline ctermfg=green               guifg=green

  set ts=8

  " Open a window with the man page for the word under the cursor
  " <http://vim.wikia.com/wiki/Open_a_window_with_the_man_page_for_the_word_under_the_cursor>
  " <http://www.derekwyatt.org/vim/the-vimrc-file/walking-around-your-windows/>
  fun! ReadMan()
    let s:man_word = expand('<cword>')
    :exe ":wincmd n"
    :exe ":r!man " . s:man_word . " | col -b -x | $FS/bin/vi -R -S /etc/vim/man.vim -"
    :close
  endfun

  " Map the K key to the ReadMan function
  map K :call ReadMan()<CR>

" EOF
