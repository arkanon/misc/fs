<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>autoload/xolox.vim</title>
<meta name="Generator" content="Vim/7.3">
<meta name="plugin-version" content="vim7.3_v6">
<meta name="syntax" content="vim">
<meta name="settings" content="use_css,ignore_folding,number_lines">
<style type="text/css">
<!--
pre { font-family: monospace; color: #000000; background-color: #ffffff; }
body { font-family: monospace; color: #000000; background-color: #ffffff; }
.lnr { color: #656565; background-color: #ffffff; }
.Type { color: #1d318d; background-color: #ffffff; font-weight: bold; }
.Special { color: #844631; background-color: #ffffff; }
.Identifier { color: #0e7c6b; background-color: #ffffff; }
.Statement { color: #2239a8; background-color: #ffffff; font-weight: bold; }
.Constant { color: #a8660d; background-color: #ffffff; }
.Underlined { color: #272fc2; background-color: #ffffff; text-decoration: underline; }
.PreProc { color: #a33243; background-color: #ffffff; }
.Comment { color: #558817; background-color: #ffffff; }

html, body, pre { margin: 0; padding: 0; }
a:link, a:visited { color: inherit; text-decoration: none; }
pre:hover a:link, pre:hover a:visited { text-decoration: underline; }
a:link span, a:visited span { text-decoration: inherit; }
.lnr a:link, .lnr a:visited { text-decoration: none !important; }
-->
</style>
</head>
<body>
<pre>
<span class="lnr"><a name="l1" href="#l1">  1 </a></span><span class="Comment">&quot; Vim script</span>
<span class="lnr"><a name="l2" href="#l2">  2 </a></span><span class="Comment">&quot; </span><span class="PreProc">Maintainer:</span><span class="Comment"> Peter Odding &lt;</span><span class="Underlined">peter@peterodding.com</span><span class="Comment">&gt;</span>
<span class="lnr"><a name="l3" href="#l3">  3 </a></span><span class="Comment">&quot; </span><span class="PreProc">Last Change:</span><span class="Comment"> August 30, 2010</span>
<span class="lnr"><a name="l4" href="#l4">  4 </a></span><span class="Comment">&quot; </span><span class="PreProc">URL:</span><span class="Comment"> </span><span class="Underlined"><a href="http://peterodding.com/code/vim/profile/autoload/xolox.vim">http://peterodding.com/code/vim/profile/autoload/xolox.vim</a></span>
<span class="lnr"><a name="l5" href="#l5">  5 </a></span>
<span class="lnr"><a name="l6" href="#l6">  6 </a></span><span class="Comment">&quot; Miscellaneous functions used throughout my Vim profile and plug-ins.</span>
<span class="lnr"><a name="l7" href="#l7">  7 </a></span>
<span class="lnr"><a name="l8" href="#l8">  8 </a></span><span class="Comment">&quot; Lately I've been losing my message history a lot so I've added this option</span>
<span class="lnr"><a name="l9" href="#l9">  9 </a></span><span class="Comment">&quot; which keeps a ring buffer with the last N messages in </span><span class="Constant">&quot;<a href="xolox.vim#l15">g:xolox_messages</a>&quot;</span><span class="Comment">.</span>
<span class="lnr"><a name="l10" href="#l10"> 10 </a></span><span class="Statement">if</span> !<span class="Identifier">exists</span><span class="Special">(</span><span class="Constant">'<a href="xolox.vim#l11">g:xolox_message_buffer</a>'</span><span class="Special">)</span>
<span class="lnr"><a name="l11" href="#l11"> 11 </a></span>  <span class="Statement">let</span> <a href="xolox.vim#l11">g:xolox_message_buffer</a> <span class="Statement">=</span> <span class="Constant">100</span>
<span class="lnr"><a name="l12" href="#l12"> 12 </a></span><span class="Statement">endif</span>
<span class="lnr"><a name="l13" href="#l13"> 13 </a></span>
<span class="lnr"><a name="l14" href="#l14"> 14 </a></span><span class="Statement">if</span> !<span class="Identifier">exists</span><span class="Special">(</span><span class="Constant">'<a href="xolox.vim#l15">g:xolox_messages</a>'</span><span class="Special">)</span>
<span class="lnr"><a name="l15" href="#l15"> 15 </a></span>  <span class="Statement">let</span> <a href="xolox.vim#l15">g:xolox_messages</a> <span class="Statement">=</span> []
<span class="lnr"><a name="l16" href="#l16"> 16 </a></span><span class="Statement">endif</span>
<span class="lnr"><a name="l17" href="#l17"> 17 </a></span>
<span class="lnr"><a name="l18" href="#l18"> 18 </a></span><span class="Statement">function</span>! <span class="Identifier">xolox#trim</span><span class="Special">(</span>s<span class="Special">)</span> <span class="Comment">&quot; -- trim whitespace from start/end of </span><span class="Special">{s}</span><span class="Comment"> {{{1</span>
<span class="lnr"><a name="l19" href="#l19"> 19 </a></span>  <span class="Statement">return</span> <span class="Identifier">substitute</span><span class="Special">(</span>a:s, <span class="Constant">'^\s*\(.\{-}\)\s*$'</span>, <span class="Constant">'\1'</span>, <span class="Constant">''</span><span class="Special">)</span>
<span class="lnr"><a name="l20" href="#l20"> 20 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l21" href="#l21"> 21 </a></span>
<span class="lnr"><a name="l22" href="#l22"> 22 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l22">xolox#quote_pattern</a></span><span class="Special">(</span>s<span class="Special">)</span> <span class="Comment">&quot; -- convert </span><span class="Special">{s}</span><span class="Comment"> to pattern that matches </span><span class="Special">{s}</span><span class="Comment"> literally (on word boundaries!) {{{1</span>
<span class="lnr"><a name="l23" href="#l23"> 23 </a></span>  <span class="Statement">let</span> patt <span class="Statement">=</span> <span class="Identifier"><a href="xolox/escape.vim#l8">xolox#escape#pattern</a></span><span class="Special">(</span>a:s<span class="Special">)</span>
<span class="lnr"><a name="l24" href="#l24"> 24 </a></span>  <span class="Statement">if</span> patt <span class="Statement">=~</span> <span class="Constant">'^\w'</span>
<span class="lnr"><a name="l25" href="#l25"> 25 </a></span>    <span class="Statement">let</span> patt <span class="Statement">=</span> <span class="Constant">'\&lt;'</span> <span class="Statement">.</span> patt
<span class="lnr"><a name="l26" href="#l26"> 26 </a></span>  <span class="Statement">endif</span>
<span class="lnr"><a name="l27" href="#l27"> 27 </a></span>  <span class="Statement">if</span> patt <span class="Statement">=~</span> <span class="Constant">'\w$'</span>
<span class="lnr"><a name="l28" href="#l28"> 28 </a></span>    <span class="Statement">let</span> patt <span class="Statement">=</span> patt <span class="Statement">.</span> <span class="Constant">'\&gt;'</span>
<span class="lnr"><a name="l29" href="#l29"> 29 </a></span>  <span class="Statement">endif</span>
<span class="lnr"><a name="l30" href="#l30"> 30 </a></span>  <span class="Statement">return</span> patt
<span class="lnr"><a name="l31" href="#l31"> 31 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l32" href="#l32"> 32 </a></span>
<span class="lnr"><a name="l33" href="#l33"> 33 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l33">xolox#unique</a></span><span class="Special">(</span>list<span class="Special">)</span> <span class="Comment">&quot; -- remove duplicate values from </span><span class="Special">{list}</span><span class="Comment"> (in-place) {{{1</span>
<span class="lnr"><a name="l34" href="#l34"> 34 </a></span>  <span class="Statement">let</span> index <span class="Statement">=</span> <span class="Constant">0</span>
<span class="lnr"><a name="l35" href="#l35"> 35 </a></span>  <span class="Statement">while</span> index <span class="Statement">&lt;</span> <span class="Identifier">len</span><span class="Special">(</span>a:list<span class="Special">)</span>
<span class="lnr"><a name="l36" href="#l36"> 36 </a></span>    <span class="Statement">let</span> value <span class="Statement">=</span> a:list[index]
<span class="lnr"><a name="l37" href="#l37"> 37 </a></span>    <span class="Statement">let</span> match <span class="Statement">=</span> <span class="Identifier">index</span><span class="Special">(</span>a:list, value, index<span class="Statement">+</span><span class="Constant">1</span><span class="Special">)</span>
<span class="lnr"><a name="l38" href="#l38"> 38 </a></span>    <span class="Statement">if</span> <span class="Statement">match</span> <span class="Statement">&gt;=</span> <span class="Constant">0</span>
<span class="lnr"><a name="l39" href="#l39"> 39 </a></span>      <span class="Statement">call</span> <span class="Identifier">remove</span><span class="Special">(</span>a:list, match<span class="Special">)</span>
<span class="lnr"><a name="l40" href="#l40"> 40 </a></span>    <span class="Statement">else</span>
<span class="lnr"><a name="l41" href="#l41"> 41 </a></span>      <span class="Statement">let</span> index <span class="Statement">+=</span> <span class="Constant">1</span>
<span class="lnr"><a name="l42" href="#l42"> 42 </a></span>    <span class="Statement">endif</span>
<span class="lnr"><a name="l43" href="#l43"> 43 </a></span>    <span class="Statement">unlet</span> value
<span class="lnr"><a name="l44" href="#l44"> 44 </a></span>  <span class="Statement">endwhile</span>
<span class="lnr"><a name="l45" href="#l45"> 45 </a></span>  <span class="Statement">return</span> a:list
<span class="lnr"><a name="l46" href="#l46"> 46 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l47" href="#l47"> 47 </a></span>
<span class="lnr"><a name="l48" href="#l48"> 48 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l48">xolox#message</a></span><span class="Special">(</span><span class="Statement"><a href="../vimrc#l512">...</a></span><span class="Special">)</span> <span class="Comment">&quot; -- show a formatted informational message to the user {{{1</span>
<span class="lnr"><a name="l49" href="#l49"> 49 </a></span>  <span class="Statement">call</span> <span class="Identifier"><a href="xolox.vim#l62">s:message</a></span><span class="Special">(</span><span class="Constant">'title'</span>, a:000<span class="Special">)</span>
<span class="lnr"><a name="l50" href="#l50"> 50 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l51" href="#l51"> 51 </a></span>
<span class="lnr"><a name="l52" href="#l52"> 52 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l52">xolox#warning</a></span><span class="Special">(</span><span class="Statement"><a href="../vimrc#l512">...</a></span><span class="Special">)</span> <span class="Comment">&quot; -- show a formatted warning message to the user {{{1</span>
<span class="lnr"><a name="l53" href="#l53"> 53 </a></span>  <span class="Statement">call</span> <span class="Identifier"><a href="xolox.vim#l62">s:message</a></span><span class="Special">(</span><span class="Constant">'warningmsg'</span>, a:000<span class="Special">)</span>
<span class="lnr"><a name="l54" href="#l54"> 54 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l55" href="#l55"> 55 </a></span>
<span class="lnr"><a name="l56" href="#l56"> 56 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l56">xolox#debug</a></span><span class="Special">(</span><span class="Statement"><a href="../vimrc#l512">...</a></span><span class="Special">)</span> <span class="Comment">&quot; -- show a formatted debugging message to the user {{{1</span>
<span class="lnr"><a name="l57" href="#l57"> 57 </a></span>  <span class="Statement">if</span> <span class="PreProc">&amp;vbs</span> <span class="Statement">&gt;=</span> <span class="Constant">1</span>
<span class="lnr"><a name="l58" href="#l58"> 58 </a></span>    <span class="Statement">call</span> <span class="Identifier"><a href="xolox.vim#l62">s:message</a></span><span class="Special">(</span><span class="Constant">'question'</span>, a:000<span class="Special">)</span>
<span class="lnr"><a name="l59" href="#l59"> 59 </a></span>  <span class="Statement">endif</span>
<span class="lnr"><a name="l60" href="#l60"> 60 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l61" href="#l61"> 61 </a></span>
<span class="lnr"><a name="l62" href="#l62"> 62 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l62">s:message</a></span><span class="Special">(</span>hlgroup, args<span class="Special">)</span> <span class="Comment">&quot; -- implementation of </span><span class="Identifier">message()</span><span class="Comment"> and </span><span class="Identifier">warning()</span><span class="Comment"> {{{1</span>
<span class="lnr"><a name="l63" href="#l63"> 63 </a></span>  <span class="Statement">let</span> nargs <span class="Statement">=</span> <span class="Identifier">len</span><span class="Special">(</span>a:args<span class="Special">)</span>
<span class="lnr"><a name="l64" href="#l64"> 64 </a></span>  <span class="Statement">if</span> nargs <span class="Statement">==</span> <span class="Constant">1</span>
<span class="lnr"><a name="l65" href="#l65"> 65 </a></span>    <span class="Statement">let</span> message <span class="Statement">=</span> a:args[<span class="Constant">0</span>]
<span class="lnr"><a name="l66" href="#l66"> 66 </a></span>  <span class="Statement">elseif</span> nargs <span class="Statement">&gt;=</span> <span class="Constant">2</span>
<span class="lnr"><a name="l67" href="#l67"> 67 </a></span>    <span class="Statement">let</span> message <span class="Statement">=</span> <span class="Identifier">call</span><span class="Special">(</span><span class="Constant">'printf'</span>, a:args<span class="Special">)</span>
<span class="lnr"><a name="l68" href="#l68"> 68 </a></span>  <span class="Statement">endif</span>
<span class="lnr"><a name="l69" href="#l69"> 69 </a></span>  <span class="Statement">if</span> <span class="Identifier">exists</span><span class="Special">(</span><span class="Constant">'message'</span><span class="Special">)</span>
<span class="lnr"><a name="l70" href="#l70"> 70 </a></span>    <span class="Statement">try</span>
<span class="lnr"><a name="l71" href="#l71"> 71 </a></span><span class="Comment">      &quot; Temporarily disable Vim's |hit-enter| prompt and mode display.</span>
<span class="lnr"><a name="l72" href="#l72"> 72 </a></span>      <span class="Statement">if</span> !<span class="Identifier">exists</span><span class="Special">(</span><span class="Constant">'</span><a href="xolox.vim#l73">s:more_save</a><span class="Constant">'</span><span class="Special">)</span>
<span class="lnr"><a name="l73" href="#l73"> 73 </a></span>        <span class="Statement">let</span> <span class="Identifier"><a href="xolox.vim#l73">s:more_save</a></span> <span class="Statement">=</span> <span class="PreProc">&amp;more</span>
<span class="lnr"><a name="l74" href="#l74"> 74 </a></span>        <span class="Statement">let</span> <span class="Identifier"><a href="xolox.vim#l74">s:ruler_save</a></span> <span class="Statement">=</span> <span class="PreProc">&amp;ruler</span>
<span class="lnr"><a name="l75" href="#l75"> 75 </a></span>        <span class="Statement">let</span> <span class="Identifier"><a href="xolox.vim#l75">s:smd_save</a></span> <span class="Statement">=</span> <span class="PreProc">&amp;showmode</span>
<span class="lnr"><a name="l76" href="#l76"> 76 </a></span>      <span class="Statement">endif</span>
<span class="lnr"><a name="l77" href="#l77"> 77 </a></span>      <span class="Statement">set</span> <span class="PreProc">nomore</span> <span class="PreProc">noshowmode</span>
<span class="lnr"><a name="l78" href="#l78"> 78 </a></span>      <span class="Statement">if</span> <span class="Identifier">winnr</span><span class="Special">(</span><span class="Constant">'$'</span><span class="Special">)</span> <span class="Statement">==</span> <span class="Constant">1</span> | <span class="Statement">set</span> <span class="PreProc">noruler</span> | endif
<span class="lnr"><a name="l79" href="#l79"> 79 </a></span>      <span class="Statement">augroup</span> <span class="Type"><a href="xolox.vim#l79">PluginXoloxHideMode</a></span>
<span class="lnr"><a name="l80" href="#l80"> 80 </a></span>        <span class="Statement">autocmd</span>! <span class="Type">CursorHold</span>,<span class="Type">CursorHoldI</span> * <span class="Statement">call</span> <span class="Identifier"><a href="xolox.vim#l98">s:clear_message</a></span><span class="Special">()</span>
<span class="lnr"><a name="l81" href="#l81"> 81 </a></span>      <span class="Statement">augroup</span> <span class="Type"><a href="xolox.vim#l81">END</a></span>
<span class="lnr"><a name="l82" href="#l82"> 82 </a></span>      <span class="Statement">execute</span> <span class="Constant">'echohl'</span> a:hlgroup
<span class="lnr"><a name="l83" href="#l83"> 83 </a></span><span class="Comment">      &quot; Redraw to avoid |hit-enter| prompt.</span>
<span class="lnr"><a name="l84" href="#l84"> 84 </a></span>      <span class="Statement">redraw</span> | <span class="Statement">echomsg</span> message
<span class="lnr"><a name="l85" href="#l85"> 85 </a></span>      <span class="Statement">if</span> <a href="xolox.vim#l11">g:xolox_message_buffer</a> <span class="Statement">&gt;</span> <span class="Constant">0</span>
<span class="lnr"><a name="l86" href="#l86"> 86 </a></span>        <span class="Statement">call</span> <span class="Identifier">add</span><span class="Special">(</span><a href="xolox.vim#l15">g:xolox_messages</a>, message<span class="Special">)</span>
<span class="lnr"><a name="l87" href="#l87"> 87 </a></span>        <span class="Statement">if</span> <span class="Identifier">len</span><span class="Special">(</span><a href="xolox.vim#l15">g:xolox_messages</a><span class="Special">)</span> <span class="Statement">&gt;</span> <a href="xolox.vim#l11">g:xolox_message_buffer</a>
<span class="lnr"><a name="l88" href="#l88"> 88 </a></span>          <span class="Statement">call</span> <span class="Identifier">remove</span><span class="Special">(</span><a href="xolox.vim#l15">g:xolox_messages</a>, <span class="Constant">0</span><span class="Special">)</span>
<span class="lnr"><a name="l89" href="#l89"> 89 </a></span>        <span class="Statement">endif</span>
<span class="lnr"><a name="l90" href="#l90"> 90 </a></span>      <span class="Statement">endif</span>
<span class="lnr"><a name="l91" href="#l91"> 91 </a></span>    <span class="Statement">finally</span>
<span class="lnr"><a name="l92" href="#l92"> 92 </a></span><span class="Comment">      &quot; Always clear message highlighting -- even when interrupted by Ctrl-C.</span>
<span class="lnr"><a name="l93" href="#l93"> 93 </a></span>      <span class="Statement">echohl</span> <span class="Type">none</span>
<span class="lnr"><a name="l94" href="#l94"> 94 </a></span>    <span class="Statement">endtry</span>
<span class="lnr"><a name="l95" href="#l95"> 95 </a></span>  <span class="Statement">endif</span>
<span class="lnr"><a name="l96" href="#l96"> 96 </a></span><span class="Statement">endfunction</span>
<span class="lnr"><a name="l97" href="#l97"> 97 </a></span>
<span class="lnr"><a name="l98" href="#l98"> 98 </a></span><span class="Statement">function</span>! <span class="Identifier"><a href="xolox.vim#l98">s:clear_message</a></span><span class="Special">()</span>
<span class="lnr"><a name="l99" href="#l99"> 99 </a></span>  <span class="Statement">echo</span> <span class="Constant">''</span>
<span class="lnr"><a name="l100" href="#l100">100 </a></span>  <span class="Statement">let</span> <span class="PreProc">&amp;more</span> <span class="Statement">=</span> <a href="xolox.vim#l73">s:more_save</a>
<span class="lnr"><a name="l101" href="#l101">101 </a></span>  <span class="Statement">let</span> <span class="PreProc">&amp;showmode</span> <span class="Statement">=</span> <a href="xolox.vim#l75">s:smd_save</a>
<span class="lnr"><a name="l102" href="#l102">102 </a></span>  <span class="Statement">let</span> <span class="PreProc">&amp;ruler</span> <span class="Statement">=</span> <a href="xolox.vim#l74">s:ruler_save</a>
<span class="lnr"><a name="l103" href="#l103">103 </a></span>  <span class="Statement">unlet</span> <span class="Identifier"><a href="xolox.vim#l73">s:more_save</a></span> <a href="xolox.vim#l74">s:ruler_save</a> <a href="xolox.vim#l75">s:smd_save</a>
<span class="lnr"><a name="l104" href="#l104">104 </a></span>  <span class="Statement">autocmd</span>! <span class="Type"><a href="xolox.vim#l79">PluginXoloxHideMode</a></span>
<span class="lnr"><a name="l105" href="#l105">105 </a></span>  <span class="Statement">augroup</span>! <span class="Type"><a href="xolox.vim#l79">PluginXoloxHideMode</a></span>
<span class="lnr"><a name="l106" href="#l106">106 </a></span><span class="Statement">endfunction</span>
</pre>
</body>
</html>
