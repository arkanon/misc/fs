# /etc/profile.d/custom.sh

# procurar tag [ CONTINUAR TODO WORKAROUND BUG ATENÇÃO SNAP ]

# Arkanon <arkanon@lsd.org.br>
# 2022/02/21 (Mon) 11:08:45 -03
# 2016/06/17 (Sex) 11:13:10 BRS



  export    CUSTOMIZE=~/.if_from-customize
  export RUN_ABSTRACT=~/.if_from-run_abstract
  export   SET_PROMPT=~/.if_from-set_prompt
  export      SET_VIM=~/.if_from-set_vim
  export   USE_SCREEN=~/.if_from-use_screen
  export   LOG_SCREEN=~/.if_from-log_screen

  export        SHELL=/bin/bash
  export          TTY=$( ( LC_ALL=C tty | grep -v not || echo unk ) | cut -d/ -f3- )
  export    ORIGIN_IP=$( ( who --ips || w -i || who ) 2> /dev/null | grep -E "$TTY\b" | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}' || echo 127.0.0.1 )
  export    ORIGIN_HN=$( ( timeout 1 host $ORIGIN_IP || timeout 1 host $ORIGIN_IP 172.23.1.10 ) 2> /dev/null | grep 'domain name' | cut -d\  -f5 | cut -d. -f1 ) # add hostnames by line in ~/.if_from-* files

  alias     customize='export CUSTOMIZE_NOW=true; rer'

  rer()
  {
  # unset CUSTOM_SOURCED
    . /etc/profile
    if [ -e ~/.bash_profile ]
    then
      . ~/.bash_profile
    else
       [ -e ~/.profile    ] && . ~/.profile
       [ -e ~/.bash_login ] && . ~/.bash_login
    fi
  }

  is_from_set()
  {
    local from=$(sed -r 's/( |#.*)//g;/^$/d' "$1" 2> /dev/null)
    $CUSTOMIZE_NOW || [[ $from ]] && \
    {
      [[ $CUSTOMIZE_NOW == true ]] || \
      grep -qxE "$(echo -n "$(grep -v / <<< "$from")" | tr "\n" "|")" <<< "$ORIGIN_HN" || \
      grep -qxF "$ORIGIN_IP" <<< "$from" || \
      for nm in $(grep / <<< "$from"); { nmap -nsL $nm | grep report | cut -d\  -f5; } | grep -qxF "$ORIGIN_IP"
    }
  }



# ======================================================================================================================== #
  is_from_set $CUSTOMIZE || return # ===================================================================================== #
# ======================================================================================================================== #



  export      DEFAULT_GW=$( route -n 2> /dev/null | grep -E ^0.0.0.0 | sed -r 's/ +/\t/g' | cut -f2 )
# export        HOSTNAME=$( cat /etc/hostname 2> /dev/null || (. /etc/sysconfig/network && echo $HOSTNAME) )

  export      TIMEFORMAT=%lR
# export         DISPLAY=${DISPLAY:-:0}
  export          EDITOR=vim
  export            LESS=-RMSNXFJ
  export        LESSOPEN="| /fs/bin/src-hilite-lesspipe.sh %s"
# export     LESSCHARSET=utf8



  export            PATH=/fs/bin:$PATH:$HOME/bin:$HOME/.local/bin
  ((UID==0)) &&     PATH=/fs/sbin:/scripts:$PATH
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/fs/lib:$HOME/lib:$HOME/.local/lib



  read osr < /proc/sys/kernel/osrelease

  if [[ $osr =~ .*-Microsoft$ ]]
  then

    winenv()
    {
      [[ $1 =~ [wu] ]] || { echo erro; return; }
      format=$1; shift
      local a=${*:-.*}
      [[ $format = w ]] \
      && sed=' s/=/="/ ; s/$/"/ ' \
      || sed='
               /drive=.:$/Is/.*/&\\/
               s/=/="/
               s/$/"/
               /:\\/s/[";]([^";]+)/:$(wslpath -u "\1")/g
               /"\\[^:]/s|\\|/|g
               s/=:/="/g
               s/^([^(=]*)\(/\1_/
               s/^([^)=]*)\)/\1_/
               s/\\/\\\\/g
               s/="/=\\"/
               s/"$/\\"/
               s/\$[^(]/\\&/g
             '
      list=$(cmd.exe /c set 2> /dev/null | fromdos | sed -r "$sed" | grep -iE "^(${a// /|})=")
    # echo "$list"
      [[ $format = w ]] && echo "$list" || eval "echo \"$list\"" | sed -r '/drive=.+\/"$/Is|(.+)/|\1|'
    }

    eval $(winenv u ProgramFiles)
    PATH=$PATH:"$ProgramFiles/Vim/vim82"
    PATH=$PATH:"$ProgramFiles/LibreOffice/program"
    PATH=$PATH:"$ProgramFiles/Microsoft Office/root/Office16"

  fi



  shopt -s checkwinsize
  shopt -u progcomp # complete path without escape or expand variables



  unset  LANG
  unset  LANGUAGE
  unset  LC_ALL
  unset  LC_ADDRESS
  unset  LC_COLLATE
  unset  LC_CTYPE
  unset  LC_IDENTIFICATION
  unset  LC_MEASUREMENT
  unset  LC_MESSAGES
  unset  LC_MONETARY
  unset  LC_NAME
  unset  LC_NUMERIC
  unset  LC_PAPER
  unset  LC_TELEPHONE
  unset  LC_TIME

  export       LANG='pt_BR.UTF-8'
  export   LANGUAGE='pt_BR.UTF-8'
# export     LC_ALL='pt_BR.UTF-8'
  export LC_COLLATE='C'

  export COLUMNS LINES

  export NO_AT_BRIDGE=1 # ** (indicator-multiload:5675): WARNING **: Couldn't connect to accessibility bus: Failed to connect to socket /tmp/dbus-0qzug6cbOI: Conexão recusada

# f=/fs/etc/vim/man.vim
# if [ -e $f ]
# then
#   vim -h | grep -q not-a-term && nat=--not-a-term || nat=
#   PAGER="bash -c 'col -b -x | vim        $nat -R -S $f -'" # <http://vim.wikia.com/wiki/Using_vim_as_a_man-page_viewer_under_Unix>
# # PAGER="bash -c 'col -b -x | /fs/bin/vi $nat -R -S $f -'"
# fi
# which most &> /dev/null && MANPAGER="most -s" # PAGER ou MANPAGER
# export MANPAGER
  export             MANPATH=/fs/man:/usr/local/share/man:/usr/share/man
  export MAN_DISABLE_SECCOMP=1
  # workaround bug man-db em 2018/02/05 (Mon) <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=889617#32>
  #  LC_ALL=C man man
  #   man:
  #    command exited with status 4:
  #     /usr/lib/man-db/zsoelim \
  #      | /usr/lib/man-db/manconv -f UTF-8:ISO-8859-1 -t UTF-8//IGNORE \
  #      | preconv -e UTF-8 \
  #      | tbl \
  #      | nroff -mandoc -rLL=142n -rLT=142n -Tascii

  # <http://linuxquestions.org/questions/linux-software-2/error-converting-text-from-im-to-utf-8-a-927013/#post4590993>
# export GTK_IM_MODULE=xim
  unset  GTK_IM_MODULE



  umask 2002 2> /dev/null || umask 002

  # <http://gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html>
  shopt -s cmdhist    # save all lines of a multiple-line command in the same history entry
  shopt -s histappend # append history lines to history file at exit, instead of overwritting the file
  shopt -s histreedit # give the opportunity to re-edit a failed history substitution
  shopt -s histverify # results of history substitution are not immediately passed to the shell parser, loading it to the buffer for further modification

  export           HISTTIMEFORMAT='%Y/%m/%d %a %T %z  '
  export              HISTCONTROL=             # reset behaviour to save duplicates and space started commands to history
  export                 HISTSIZE=             # unlimited history
  export             HISTFILESIZE=             # unlimited history file
  export           PROMPT_COMMAND='history -a; sp t ${PWD##*/}' # append each command line to history file immediately after its execution (prevents loss of commands in multiple sessions)
  export command_oriented_history=1            # save all lines of a multiple-line command (eg: while/for loop) in a single history entry

  alias                         h='history'



  noh()
  {

    [ "$DEBUG" ] && echo "# args: $*"
    unset last_arg
    for last_arg; { true; } # <http://stackoverflow.com/a/1853993>
    args=("$@")
    [[ $last_arg == go ]] && unset "args[${#args[@]}-1]" # <http://stackoverflow.com/a/20398578>

    if [[ $* == -h ]]
    then
      [ "$DEBUG" ] && echo "# 1"
      echo -e "\nApaga do histórico comandos por número de linha ou por expressão regular.\nSeu funcionamento depende do histórico em memória dos outros terminais abertos.\n\nUso: $FUNCNAME [-h|regexp|cmd# cmd# ...] [go]\n\nSem o parâmetro 'go', as linhas que seriam removidas são apenas mostradas.\n"
      [ "$DEBUG" ] \
      && echo -e "MODO DEBUG ATIVADO\n\nComandos que alteram o histórico serão apenas mostrados ao invés de serem executados.\n[ unset DEBUG ] desativa esse modo." \
      || echo -e "MODO DEBUG DESATIVADO\n\nCom [ export DEBUG=echo ] os comandos que alteram o histórico serão apenas mostrados ao invés de executados."
      echo
      noh

    elif [[ ${args[*]} =~ ^[0-9\ ]+$ ]]
    then
      if [[ $last_arg == go ]]
      then
        [ "$DEBUG" ] && echo "# 2"
        for i in $(printf "%s\n" ${args[*]} | sort -ru); { $DEBUG history -d $i; sleep .01; }
        noh
        history -w
      else
        [ "$DEBUG" ] && echo "# 3"
        args=${args[*]}
        match=$(history | grep -wE "^ *(${args// /|}) ")
        echo "$match"
        echo "# lines: $(wc -l <<< "$match")"
        noh
      fi

    elif [[ "$*" != "" ]]
    then
      match=$(history | grep -E "${args[*]}")
      if [[ $last_arg == go ]]
      then
        [ "$DEBUG" ] && echo "# 4"
        $DEBUG $FUNCNAME $(grep -oE '^ +[0-9]+ ' <<< "$match") go
      else
        [ "$DEBUG" ] && echo "# 5"
        echo "$match"
        echo "# lines: $(wc -l <<< "$match")"
      fi

    else
      [ "$DEBUG" ] && echo "# 6"
      [[ $BASH_VERSION =~ ^(5\.(1[0-9]|[1-9])|1[0-9]|[6-9]) ]] # BASH_VERSION >= 5.1
      $DEBUG history -d $((HISTCMD-$?))
    fi

  }



  for f in '' /{etc,fs/etc,scripts}/{DIRCOLORS,DIR_COLORS} $HOME/.{dircolors,dir_colors}
  {
    [ -e $f ] && . <(dircolors -b $f)
  }

  alias          l="LC_ALL= LC_NUMERIC=pt_BR.UTF-8 BLOCK_SIZE=\'1 ls -lapisvT0 --quoting-style=shell --color=always --time-style='+%Y/%m/%d %a %T %:::z' --group-directories-first"
# alias          l="LC_ALL= LC_NUMERIC=pt_BR.UTF-8 BLOCK_SIZE=\'1 ls -lapisvT0 --hyperlink --quoting-style=shell --color=always --time-style='+%Y/%m/%d %a %T %:::z' --group-directories-first"

  alias          p="printf '\e[?2004l'" # select-paste adds 0~ and 1~ <http://unix.stackexchange.com/a/196574>

# alias         wi='cut -c-$((COLUMNS-1))' # width
  alias         wi='less -RSEX' # width

  alias        du0='du -chsx'
  alias        cp0='time cp --sparse=always --reflink=auto -a'
  alias      date0='date +"%Y/%m/%d %a %T %Z"'
  alias     watch0='watch -pcbn1 ' # watch command alias expansion <http://unix.stackexchange.com/a/25329>
  alias     rsync0='time rsync -vaAShHxXPiz -f "- lost+found"'
# alias     lsblk0='lsblk -po MODEL,SIZE,NAME,PARTLABEL,LABEL,FSTYPE,MOUNTPOINT'
  alias    fbmodes="sudo hwinfo --framebuffer | grep -w Mode | gawk --non-decimal-data -F'[ x+,:()]' '{printf(\"%s\t0x%s\t%d\t%4d x %4d\t+ %4d\t%2d %s\n\",\$3,\$5,\"0x\"\$5,\$7,\$8,\$11,\$14,\$15)}' | sort -t$'\t' -k4,4nr -k6,6n"

  alias      grep0='grep --color=always'
  alias      make0='colormake'

  alias      sudo0='sudo ' # aliases not available when using sudo <http://askubuntu.com/a/22043>
  alias     please='sudo '
  alias  synaptic0='sudo -E synaptic &'
  alias      halt0='sudo halt -p'
  alias    reboot0='sudo reboot'

  alias       pubi='ping -c1 -W2 ns1.google.com &> /dev/null && dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | tr -d \"'

  alias         di='docker images | pgrep0 "1;37" ".+TAG.+|.*/\K[^/ ]+|$" | pgrep0 "1;31" ".+none.+|$" | sort'
  alias         dp='docker ps -a'
  alias         ds='docker stop'
  alias         dr='docker rm'

  dsr(){ docker stop $*; docker rm $*; }

# export PUBLIC_IP=$(pubi 2> /dev/null)



  dmesg -h 2>&1 | grep -q -- -delta &&  delta='-d'
  dmesg -h 2>&1 | grep -q decode    && decode='-x'
  dmesg -h 2>&1 | grep -q ctime     &&  ctime='-T'
  dmesg -h 2>&1 | grep -q color     &&  color='-L'
  dmesg -h 2>&1 | grep -q always    &&   when='=always'
  alias dmesg0="creds &> /dev/null; sudo dmesg $delta $decode $ctime $color$when | cat -n"



  pgrep0() { local c=${1:-1;31}; shift; GREP_COLOR=$c grep --color=always -P "$@"; }

  telnet0() { while true; do timeout ${3:-2} curl -v telnet://$1:$2 <<< exit &> /dev/null && { echo sucesso; break; }; echo erro; sleep ${4:-0}; done; }

  turl() { eval curl --socks5-hostname localhost:9150 ${@:--s https://check.torproject.org | lynx -dump -nolist -nomargins -stdin; echo -e "\"\n>>> Proteja URLs com parâmetros entre apóstrofos e aspas. <<<\n\""}; }



  vi()
  {
    local f s m l t v # p
     f=$1
     s=${_V:-${f##*/}}
    _V=${_V:-VIM_$RANDOM} # não deve ser local para precisar ser setada apenas uma vez
    { grep -q -- -h <<< $* && [[   $_V ]]; } && m="\nVIm selecionado: ${_V^^}\n"
    { grep -q -- -h <<< $* || [[ ! $_V ]]; } && \
    {
      l=$(vim --serverlist | sed 's/^/  /')
      echo -e "$m\nVIM's já abertos:\n\n$l\n\nDefina um VIm com\n\n  _V=<label>\n"
      return
    }
    shift
    t=$(echo $*)
  # p=$([[ -e $f ]] && readlink -f $f || echo .)
    f=$([[    $f ]] && echo --remote-tab-silent $f)
    v="gvim -c 'set shortmess+=A' -c 'let &titlestring=\"$s\"' --servername $_V"
    eval "$v $f"
  # while [ ! -e ${p%/*}/.${p##*/}.swp ]; do sleep .1; done
    while ! vim --serverlist | grep -qi $_V; do sleep 1; done
    [[ $t ]] && eval "$v --remote-send ':TabooRename $t<cr>'"
  }
  export -f vi



  column0()
  {
    man -P cat column 2> /dev/null | grep -qE '^ +-n ' && nomer='-n'
    man -P cat column 2> /dev/null | grep -qE '^ +-e ' && empty='-e'
    LC_ALL=C.UTF-8 column $empty $nomer -ts ${1:-$'\t'}
  }
  alias c="column0"



  id0()
  {
    LC_ALL=C id $1 \
    | tr \  '\n' \
    | sed -r 's/,/\ngroups=/g;s/[=(]/\t/g;s/\)//g;s/^uid/UID/' \
    | LC_ALL=C sort -t$'\t' -k1,1 -k3,3 \
    | sed 's/^UID/uid/'
  }



  last0()
  {
    case $* in
    '')
        time=$(last -h 2> /dev/null | grep -q time-format && echo "--time-format=iso" || echo "-F")
        last -wxa $time 2> /dev/null \
        | head -n-2 \
        | tac \
        | sed -r '
                   s/([a-z])  ([0-9]) /\1 0\2 /g             # sysvinit-tools 2.87
                   s/ +/\t/
                   s/  +/\t/g
                   s/ \(/\t\(/
                   s/([0-9])T([0-9])/\1 \2/g
                   s/([0-9]{4}) - ([A-Za-z])/\1\t\2/g        # sysvinit-tools 2.87
                   s/([0-9])([-+][0-9]{4})( - |\t)/\1 \2\t/g # util-linux     2.27.1
                   s/\t\(([^)]+)\)(\t|$)/\t\1\t/g
                   /(still|no logout)/s/(in|out|ing)\t/\1\t\t/
                   /(still|no logout)/s/(in|out|ing)$/\1\t-\t-/
                   s/\t([0-9]{2}:[0-9]{2}\t)/\t0+\1/
                   s/\t\t/\t-\t/g
                   s/\t$/\t-/g
                   s/([[:alpha:]]{3}) ([[:alpha:]]{3}) ([0-9]{2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}) ([0-9]{4})/\5-\2-\3 \4/g # sysvinit-tools 2.87
                 '
        ;;
    -l)
        {
          last0 \
          | grep -vE '(^reboot|^runlevel|crash|down|gone)' \
          | grep 'still' \
          | sort -t$'\t' -k1,1 -k4,4 -r
          last0 \
          | grep -vE '(^reboot|^runlevel|crash|down|gone|still)' \
          | sort -t$'\t' -k1,1 -k4,4 -r \
          | rev \
          | tr ' ' = \
          | uniq -f5 \
          | tr = ' ' \
          | rev
        } | cat -n \
          | cut -c5- \
          | column0
        ;;
     *)
        echo "Usage: $FUNCNAME [-l]"
    esac
  }



  diff0()
  {
    local    e=$'\033'
    local  red="$e[1;31m"
    local  gre="$e[1;32m"
    local  off="$e[0m"
    local file=($(echo *| grep -oE " [^-]+" | sed -r 's/ +$//' | grep -oE "[^ ]+$"))
    echo -e "\n$red< ${file[0]}\n$gre> ${file[1]}$off\n"
    diff $@ | colordiff
    echo
  }



  wdiff0()
  {
    local    e=$'\033'
    local  red="$e[1;31m"
    local  gre="$e[1;32m"
    local  off="$e[0m"
    local file=($(echo *| grep -oE " [^-]+" | sed -r 's/ +$//' | grep -oE "[^ ]+$"))
    echo -e "\n$red[- ${file[0]} -]\n$gre{+ ${file[1]} +}$off\n"
    wdiff -n $@ | colordiff
    echo
  }



  alias0()
  {
    echo
    alias | sed -r 's/ [[:alnum:]_]+/\x1b[1;31m&\x1b[0m/ ; s/=/\x1b[1;35m&\x1b[0m/'
    echo
    typeset -F | pgrep0 '1;31' '[^ ]+0$'
    echo
  }



  cgrep()
  {
    cat | grep --color=always -E "^|$1"
  }



  uptime0()
  {
    local ss s uptime since
    ss=$(cat /proc/uptime 2> /dev/null | cut -d\  -f1 | cut -d. -f1)
     s=s && ((ss/60/60/24==1)) && s=
    uptime=$(printf "%d day$s %02d:%02d:%02d\n" $((ss/60/60/24)) $((ss%(60*60*24)/60/60)) $((ss%(60*60)/60)) $((ss%60)))
     since=$(LC_ALL=C date -d @$(bc <<< "$(date +%s.%2N)-$(cat /proc/uptime 2> /dev/null | cut -d\  -f1)" 2> /dev/null) +"%Y/%m/%d %a %H:%M:%S" 2> /dev/null)
    echo "$uptime - since $since"
  }



  man0()
  {
    ( yelp man:$1 &> /dev/null & )
  }



  ifconfig0()
  {
    /sbin/ifconfig | grep -q Link \
    && LC_ALL=C /sbin/ifconfig \
       | grep -C1 'Link encap' \
       | grep -vx -- -- \
       | sed  -r  '/^[^ ]/{:l N;/\n./{s/\n//;bl}}' \
       | grep -w  'inet' \
       | grep     'Bcast' \
       | sed  -r  's/ +/\t/g' \
       | awk  -F'\t' '{printf"%s\tip=%s\tnm=%s\thw=%s\n",$1,$7,$9,$5}' \
       | sed  -r  's/(addr|Mask)://g' \
    || LC_ALL=C /sbin/ifconfig \
       | sed  -r  's/ +inet /\tip=/;s/ +netmask /\tnm=/;s/ +ether /\thw=/' \
       | grep -oP '(^[^ :\t]+:|\t[^ =\t]+=[^ =\t]+)' \
       | sed  -r  ':a;N;/\n[^\t]/!s/\n//;ta;P;D' \
       | sed  -r  '/nm=/!s/\t/\tnm=\t/;/ip=/!s/\t/\tip=\t/;/hw=/!s/$/\thw=/;s/:\t/\t/'
  }



  route0()
  {
    ip route \
    | sed -r 's/^(([^ ]+) )(via ([^ ]+) )?(dev ([^ ]+) ).*/\6\t\2\t\4/' \
    | column -ents$'\t' \
    | pgrep0 '1;34' '^|(wlan).*' \
    | pgrep0 '1;32' '^|(eth|ens).*' \
    | pgrep0 '1;33' '^|(docker|virbr|veth).*' \
    | pgrep0 '1;36' '^|/[0-9]+' \
    | sed -r 's/^([^m]+m)(.+)(default)/\1\2'$'\e''[1;31m\3'$'\e''[0m\1/' \
    | sort -t. -k1,1V -k2,2n -k3,3n -k4,4n
  }



  ps0()
  {
    local extra=$( [ $# = 0 ] && echo "x" || ( [ "$1" = a ] && echo "xa" || echo "U $1" ) )
    ps wwhfxao "start:1,etime:1,tty:1=TTY,rss:1=RSS(kiB),user:50,pgrp:1,pid:1,cmd" \
    | sed -r \
      '
        s/ ([0-9]+) ([0-9]+) /\t\1\t\2\t/
        s/ ([^ \t]+) *\t/\t\1\t/
        s/ ([^ \t]+\t)/\t\1/
        s/ ([^ \t]+\t)/\t\1/
        s/ ([^ \t]+\t)/\t\1/
      ' \
    | cat -n \
  # | column0
   }



  which msmtp &> /dev/null && \
  msmtp0()
  {
    local username password domain o365pass
    . $AD_C
    [ $# = 0 ] \
    && echo "echo <corpo> | $FUNCNAME <assunto> <destino1[@$domain]> <destino2@domínio> ..." \
    || {
         local    smtp=mail
         local     log=$HOME/.msmtp.log
         local    from=$USER@$HOSTNAME
         local   corpo=$(cat /dev/stdin)
         local assunto=$1; shift
         local      to=$(echo $* | tr ' ' '\n' | sed -r '/@/!s/.*/&'@$domain'/' | tr '\n' ' ') # adiciona o domínio default aos endereços da lista $to que não contiverem domínio e junta-os em uma lista separada por espaço
         echo -e "Subject: $assunto\n$corpo" | msmtp --logfile=$log --host=$smtp --from=$from $to
       }
  }



  crontab0()
  {
    local a u i e l r h f tab
    while [ "$*" ]
    do
      case $1 in
      -u) u="-u $2"
           shift;;
      -i) i="-i";;
      -e) e="-e";;
      -l) l="-l";;
      -r) r="-r";;
      -h) h="-h";;
       *) f="$1";;
      esac
      shift
    done
    if   [ "$f" ]
    then
      crontab $u $f
    elif [ "$e" ]
    then
      EDITOR=vim crontab $u $i $e
    elif [ "$r" ]
    then
      crontab $u $i $r
    elif [ "$l" ]
    then
      (
	tab=$(crontab $u $i $l 2> /dev/null)
        [ ! "$tab" ] \
        && echo none \
        || {
             echo -e " #  min\thour\tday\tmonth\tweek\tcommand"
             echo "$tab" \
             | sed -r '
                        s/(^[ \t]*|[ \t]*#.*)//g
                        /^$/d
                        s/([^ \t]+)[ \t]+/\1\t/g
                        s/\t/ /g6
                      ' \
             | cat -n   \
             | cut -c5- \
             | sed 's/\t/  /'
           }
      ) | column0 \
        | pgrep0 '1;37' '#.+$|$'
    elif [ "$h" ]
    then
      crontab $h 2>&1 \
      | tail -n+3 \
      | sed -r '
                 s/^u/U/
                 s/([[{]) /\1/g
                 s/ ([]}])/\1/g
                 s/ \| /\|/g
                 $a\        -h      (display this help and exit)
               '
    fi
  }



  free0()
  {
    local   e=$'\033'
    local whi="$e[1;37m"
    local yel="$e[1;33m"
    local off="$e[0m"
    local   u=10 # 0=k, 10=M, 20=G
    local   w=5
    [[ $* != '' ]] && grep -qvE -- '-[rs]' <<< $* && \
    {
      echo -e "
Show/free cached RAM and used SWAP areas.\n
Usage: $FUNCNAME [option]\n
  Options
    -h     this help
    -r     free only ram area
    -s     free ram and swap areas
"
      return
    }
    grep -qE -- '-[rs]' <<< $* && sudo true
    echo
    (
      echo -e "Filename\tType\tSize\tUsed\tFree\tCache" \
      | awk -F'\t' -v w=$w         '{ printf "%s\t%-4s\t%"w"s\t%"w"s\t%"w"s\t%"w"s\n",$1,$2,$3,$4,$5,$6 }'
      swapon -s \
      | sed -r 's/[ \t]+/\t/g' \
      | tail -n+2 \
      | awk -F'\t' -v w=$w -v u=$u '{ printf "%s\t%s\t%"w".0f\n",$1,$2,$3/2^u }'
      free -k \
      | grep -i swap \
      | sed -r 's/[ \t]+/\t/g' \
      | cut -f2-4 \
      | awk -F'\t' -v w=$w -v u=$u '{ printf " \tSWAP\t%"w".0f\t%"w".0f\t%"w".0f\n",$1/2^u,$2/2^u,$3/2^u }'
      free -k \
      | grep -i mem \
      | sed -r 's/[ \t]+/\t/g' \
      | cut -f 2-4,6 \
      | awk -F'\t' -v w=$w -v u=$u '{ printf " \tRAM\t%"w".0f\t%"w".0f\t%"w".0f\t%"w".0f\n",$1/2^u,$2/2^u,$3/2^u,$4/2^u }'
    ) | column0 \
      | sed -r "1s/.+/$whi&$off/;s/(RAM|SWAP +[^ ]+)/$yel&$off/"
    echo
    grep -qE -- '-[rs]' <<< $* && \
    {
      time \
      (
        TIMEFORMAT=%lR
        sync;sync
        [[ $* = -s ]] && sudo swapoff -a
        sudo bash -c "echo 3 >| /proc/sys/vm/drop_caches"
        [[ $* = -s ]] && sudo swapon  -a
        sync;sync
      )
      $FUNCNAME
    }
  }



  df0()
  {
    local  DFTO=${DFTO:-1s}
    local     e=$'\033'
    local   whi="$e[1;37m"
    local   off="$e[0m"

    local notFS="fuse{,ctl,.gvfsd-fuse},nfsd,cgroup{,2},pstore,proc,binfmt_misc,devpts,mqueue,{ns,au,sys,security,{,dev}tmp,efivar,debug,trace,config,auto,hugetlb,cgm,rpc_pipe}fs"

    echo $* | grep -q -- -a && notFS=

#   local  hpat="File"
#   local   cmd="df -hTx{$notFS} $*"
# # local   cmd="df -T $*"
# # local  notK="[^K]"

    local  hpat="FSTYPE"
    local   cmd="findmnt -lo FSTYPE,SOURCE,LABEL,SIZE,USED,AVAIL,USE%,TARGET -it \$(echo {$notFS} | tr ' ' ,)"
  # local   cmd="findmnt -D"

    local   out=$(
    (
      {
        {
          out=$(eval LC_ALL=pt_BR.UTF-8 LANGUAGE=C BLOCK_SIZE="\'1" timeout $DFTO $cmd 2> /dev/null)
          [[ $? = 124 ]] \
          && echo -e "Timedout in $DFTO. Increase time with ${whi}DFTO=value[smhd] $FUNCNAME${off}. More options with $whi--help$off." \
          || echo "$out" \
          | sed -r 's/ +/\t/' \
          | sed '$!N;s/\n\s*\t/\t/;P;D' \
          | column0 \
          | pgrep0 '1;37' "$|^$hpat.+$"                           \
          | pgrep0 '1;36' "$|^.+ /( [^/]+|$)"                     \
          | pgrep0 '1;34' "$|^.+/loop.+$"                         \
          | pgrep0 '1;35' "$|^.+/nbd.+$"                          \
          | pgrep0 '1;33' "$|^.+:[^[].+$"                         \
          | pgrep0 '1;31' "$|.* //.+$"                            \
	  | pgrep0 '1;32' "$|^[^/-0%]+ (/dev/|lxfs).+% /.+$notK$" \
          | pgrep0 '0;43' "$|^nsfs.+$"                            \
          | pgrep0 '0;41' "$|^aufs.+$"                            \
	  | pgrep0 '0;44' "$|^tmpfs.+$"                           \
          | pgrep0 '0;45' "$|^.+[^/]cgroup.+$"                    \
          | sort -r \
          | tee /dev/stderr
          # 30  black
          # 31  red
          # 32  green
          # 33  yellow
          # 34  blue
          # 35  magenta
          # 36  cyan
          # 37  white
        } 2>&3 \
        | grep -P  "$e"
      } 3>&1 1>&2 | {
          grep -Pv "$e" \
        | sort
      # | sort -Vt/ -k2
      }
    ) 2>&1
    )
    {
      echo -en "\n $whi#$off  "
      head -n 1 <<< "$out"
      tail -n+2 <<< "$out" \
      | cat -n \
      | cut -c5- \
      | sed 's/\t/  /'
      echo
    } | { grep -q -- -a <<< $* && less || cat; }
  }

  btrq()
  {
    local mnt=$1
    sudo true
    df -t btrfs | grep '^/dev' | sort | sed -r 's/ +/ /g' | awk '{print$6"\t"$1}' | uniq -f1 \
    | while read mnt dev
      do
        echo -e "\nBTRFS in $dev ($(lsblk -no LABEL $dev))\n"
        sudo btrfs quota enable $mnt
      # sudo btrfs quota rescan $mnt
        paste \
          <( sudo btrfs qgroup show --sync $mnt ) \
          <(
             echo -e "id  path\n--- ----\n"
             sudo btrfs subvolume list $mnt | cut -d\  -f2,9
           ) \
        | sed 's/^/  /'
      done
    echo
  }



  set0()
  {
    [[ $# = 0 ]] \
    && echo "
Use: $FUNCNAME v|a|A|f[ nome]|F|i|r|x

     v  variáveis
     a  arrays indexados
     A  arrays associativos
     f  funções com respectivas definições (todas ou apenas a especificada)
     F  nomes de funções
     i  variáveis declaradas como inteiro
     r  variáveis declaradas como read-only
     x  variáveis exportadas
" | pgrep0 '1;31' '\||'                                          \
  | pgrep0 '1;33' '^ +[[:alpha:]]|'                              \
  | pgrep0 '1;37' 'Use:|'                                        \
    || {
         grep -q $1 <<< aAirxfF                                  \
         && typeset -$1 $2                                       \
            | {
                [[ $1 = f ]] && sed    '1s/^/\n\n/;s/^}$/&\n\n/' \
                              | pgrep0 '1;31' '^_.+ \(\)|'       \
                              | pgrep0 '1;34' '^[^ ]+0 \(\)|'    \
                              | pgrep0 '1;37' '^[^ ]+ \(\)|'     \
                              | pgrep0 '1;35' '^[{}]| \(\)|'
                [[ $1 = F ]] && pgrep0 '1;31' ' _.+|'            \
                              | pgrep0 '1;34' ' [^ ]+0$|'        \
                              | sort                             \
                             || cat -v
              }                                                  \
         ||   {
                [[ $1 = v ]] && set -o posix && set              \
                              | cat -v
              }
       } |    {
                [[ $1 = v ]] && pgrep0 '1;37' '^[[:alnum:]_]+'   \
                              | sed    's/=/\x1b[1;35m&\x1b[0m/'
                grep -q $1 <<< aAirx && sed -r 's/ [[:alnum:]_]+/\x1b[1;37m&\x1b[0m/;s/=/\x1b[1;35m&\x1b[0m/' || cat
              } | cat -b
  }



  w0()
  {
    local   e=$'\033'
    local whi="$e[1;37m"
    local off="$e[0m"
    local out=$(
      (
        paste \
        <(
           LC_ALL=pt_BR.UTF-8 LANGUAGE=C who -H \
           | sed -r '
                      s/TIME/DATE &/
                      s/COMMENT/FROM/
                      s/([^A-Z)])$/\1 -/
                      s/[()]//g
                      s/ +/\t/g
                    '
         ) \
        <(
           LC_ALL=C PROCPS_USERLEN=32 PROCPS_FROMLEN=40 w 2> /dev/null \
           | sed -r 's/ {256}/ -/' \
           | sed -r 's/ +/\t/g;/^\t/d' \
           | cut -f 2,5
         ) \
        | awk -F'\t' '{printf"%s\t%s\t%s\t%s\t%s\t%s\n",$1,$6,$5,$3,$4,$7}'
      ) 2>&1
    )
    (
      echo -en " #  "
      head -n 1 <<< "$out"
      tail -n+2 <<< "$out" \
      | cat -n \
      | cut -c5- \
      | sed -r '
                 s/\t/  /
                 s/\t([0-9])([:.])/\t0\1\2/ # h/min com apenas 1 dígito
                 s/([0-9]+)\.[0-9]+s/00:\1/ # s com fração sem min para s com 0 min sem fração
                 s/\t([0-9:]+)m$/\t\1:00/   # h sem s
                 s/\t([0-9]{2}):([0-9]{2})$/\t00:\1:\2/ # min:s sem h
               '
    ) | column0 \
      | pgrep0 '1;37' '#.+$|$'
  }



  lsblk0()
  {
    # echo -e $'\xe2\x94'{$'\x80',$'\x94',$'\x9c'}
    # # ─ └ ├
    # echo └ | grep -P $'\xe2'$'\x94'[$'\x80'$'\x94'$'\x9c']
    # # grep: invalid UTF-8 string
    # echo └ | grep -P $'\xe2\x94\x80'\|$'\xe2\x94\x94'\|$'\xe2\x94\x9c'
    # # └
    # echo └ | grep -E '[─└├]'
    # # └
    local fields=MODEL,TRAN,SIZE,FSUSED,FSAVAIL,PARTTYPE,PARTFLAGS,FSTYPE,PTTYPE,NAME,PARTLABEL,LABEL,MOUNTPOINT
    local    out=$(lsblk -po $fields "$@")
    local    max=$(wc -L <<< "$out")
    echo
    awk \
      -v m=$max \
      -v w=$'\e[0;30;107m' \
      -v g=$'\e[0;30;47m'  \
      -v y=$'\e[0;93;47m'  \
      -v Y=$'\e[0;93m'     \
      -v b=$'\e[0;36;47m'  \
      -v B=$'\e[0;96m'     \
      -v l=$'\e[0;35;47m'  \
      -v L=$'\e[0;95m'     \
      -v o=$'\e[0m'        \
      '
        {
          b1 = ""
          bn = L
          if ( $0 ~ /gpt/   ) { bt = B } else { bt = Y }
          if ( $0 ~/^MODEL/ ) { b1 = w }
          if ( $0 ~ /sd. /  ) { b1 = g; bn = l; if ( $0 ~ /gpt/ ) { bt = b } else { bt = y } }
          Z = sprintf( "%-" m "s" , $0 )
              gsub  ( /gpt|dos/   , bt "&" o b1 , Z )
          Z = gensub( /(0x)([0-9a-f]+)/    , "\\1" bt "\\2" o b1 , "g" , Z )
          Z = gensub( / ([0-9.,]+)([A-Z])/ , " " bn "\\1" o b1 "\\2" , "g" , Z )
              gsub  ( /[─└├]/     , L  "&" o    , Z )
          printf( b1 "%s" o "\n" , Z )
        }
      ' <<< "$out" | { [[ $* == l ]] && cat || LESS= less -RSEX; }
      echo -e "\n# <http://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs>"
    echo
  }



  mount0()
  {
    local     e=$'\033'
    local   whi="$e[1;37m"
    local   off="$e[0m"
    local notFS="no{fuse{,ctl,.gvfsd-fuse},nfsd,cgroup{,2},pstore,proc,binfmt_misc,devpts,mqueue,{ns,au,sys,security,{,dev}tmp,efivar,debug,trace,config,auto,hugetlb,cgm,rpc_pipe}fs}"
    echo $* | grep -q -- -a && notFS= || notFS="-t \$(echo $notFS | tr ' ' ,)"
    local  hpat="File"
    local   cmd="mount $notFS"
    local   out=$(
      (
        {
          {
            (
              echo -e "Filesystem\tMounted on\tType\tOptions"
              eval $cmd \
              | cut -d\  -f1,3,5,6 \
              | tr -d '()' \
              | tr ' ' '\t'
            ) | column0 \
              | pgrep0 '1;37' "$|^$hpat.+$"                             \
              | pgrep0 '1;36' "$|^.+ /( [^/]+|$)"                       \
              | pgrep0 '1;34' "$|^.+/loop.+$"                           \
              | pgrep0 '1;34' "$|^.+ squashfs .+$"                      \
              | pgrep0 '1;35' "$|^.+/nbd.+$"                            \
              | pgrep0 '1;33' "$|^.+:[^[].+$"                           \
              | pgrep0 '1;31' "$|^//.+$"                                \
              | pgrep0 '1;32' "$|^(/dev/[^f][^u][^s][^e]|lxfs).+$notK$" \
              | pgrep0 '0;43' "$|^nsfs.+$"                              \
              | pgrep0 '0;41' "$|^.* aufs .+$"                          \
              | pgrep0 '0;44' "$|^.* tmpfs .+$"                         \
              | pgrep0 '0;45' "$|^.+[^/]cgroup.+$"                      \
              | sed  -r 's/^(\x1b\[1;[0-9]{2}m)(.+)([ ,])(ro)(,)/\1\2\3\x1b\[0;30;41m\4\x1b\[0m\1\5/' \
              | sort -r \
              | tee /dev/stderr
          } 2>&3 \
          | grep -P  "$e"
        } 3>&1 1>&2 | {
            grep -Pv "$e" \
          | sort -Vt/ -k2
        }
      ) 2>&1
    )
    {
      echo -en "\n $whi#$off  "
      head -n 1 <<< "$out"
      tail -n+2 <<< "$out" \
      | cat -n \
      | cut -c5- \
      | sed 's/\t/  /'
      echo
    } | { grep -q -- -a <<< $* && less -FJMRSX || less -RSEX; }
  }



  # <http://superuser.com/a/472517>
  scroll()
  {
    while read -r
    do
      echo $REPLY
      sleep ${1:-0.5}
    done
  }



  bc0()
  {

    local e=$'\033'
    local r="$e[1;31m"
    local y="$e[1;33m"
    local w="$e[1;37m"
    local o="$e[0m"
    local X=${y}x${o}
    local Y=${y}y${o}
    local Z=${y}z${o}

    if [ $# = 0 ]
    then

      cat << EOT
${w}Uso:${o} $FUNCNAME <expressão matemática bc> [# casas decimais, def=3] [${y}aₙ${o}]

${w}Operações:${o}

                   $X${r}/${o}1   inteiro de $X (indicando ${w}0${o} casas decimais)
                   $X${r}%${o}$Y   $X módulo $Y   (indicando ${w}0${o} casas decimais)
                   $X${r}^${o}$Y   $X elevado ao expoente $Y, $X não necessariamente inteiro, $Y inteiro
                  e($X)   e${r}^${o}$X
             e(l($X)${r}*${o}$Y)   $X elevado ao expoente $Y, $X e $Y, não necessariamente inteiros
               sqrt($X)   raiz quadrada de $X
             e(l($X)${r}/${o}$Y)   raiz índice $Y de $X

${w}Funções:${o}

                  s($X)   sen $X    ($X em radianos)
                  c($X)   cos $X    ($X em radianos)
             s($X)${r}/${o}c($X)   tan $X    ($X em radianos)
                  a($X)   arctan $X (retorno em radianos)
2${r}*${o}a(sqrt(1${r}-${o}$X${r}^${o}2)${r}/${o}(1${r}+${o}$X))   arccos $X (retorno em radianos)
      a($X${r}/${o}sqrt(1${r}-${o}$X${r}^${o}2))   arcsen $X (retorno em radianos)
                  l($X)   ln $X
             l($X)${r}/${o}l($Y)   logaritmo na base $Y de $X

${w}Funções Implementadas:${o}

                  i($X)   inteiro de $X (sem precisar indicar ${w}0${o} casas decimais)
                  o($X)   floor de $X
                  f($X)   fatorial de $X
                r($X,$Y)   $X arredondado para o valor mais próximo com $Y casas decimais
              s(${y}n₀${o},${y}n₁${o})   Σ${y}aₙ${o}, ${y}n₀${o}≤${y}n${o}≤${y}n₁${o}, ${y}n₀${o} e ${y}n₁${o} inteiros
              p(${y}n₀${o},${y}n₁${o})   Π${y}aₙ${o}, ${y}n₀${o}≤${y}n${o}≤${y}n₁${o}, ${y}n₀${o} e ${y}n₁${o} inteiros

${w}Conversões:${o}

     obase${r}=${o}$Y;ibase${r}=${o}$X;$Z   $Z da base $X para a base $Y
          180*$X${r}/${o}4${r}/${o}a(1)   $X de radianos para graus
          4${r}*${o}a(1)${r}*${o}$X${r}/${o}180   $X de graus para radianos

${w}Constantes:${o}

                  e(1)   e
                4${r}*${o}a(1)   π
         (sqrt(5)${r}+${o}1)${r}/${o}2   φ
         (sqrt(5)${r}-${o}1)${r}/${o}2   1${r}/${o}φ, φ${r}-${o}1

${w}Dicas:${o}
  Para evitar perda de precisão na porção decimal como em:
    ${r}$ ${o}bc0 '10${r}^${o}${w}20${o}${r}*${o}4${r}*${o}a(1)' 50
    314159265358979323846${r}.${o}264338327950288419716939937508${w}00000000000000000000 # 20 zeros${o}
  Faça algo como:
    ${r}$ ${o}bc0 'a${r}=${o}10${r}^${o}${w}20${o}${r}*${o}4${r}*${o}a(1); scale${r}=${o}50; a${r}/${o}1' 70 ${w}# pelo menos 50+20${o}
    314159265358979323846${r}.${o}26433832795028841971693993751058209749445923078164

${w}Exemplos:${o}

  ${w}# sequência de fibonacci${o}
  ${r}$ ${o}echo \$( for n in {0..20}; { bc0 "n=\$n; c=sqrt(5)       ; f=( (c+1)^n - (c-1)^n )/(c*2^n); r(f,0)" 10; } )
  ${r}$ ${o}echo \$( for n in {0..20}; { bc0 "n=\$n; g=(sqrt(5)+1)/2 ; f=(     g^n - (g-1)^n )/(2*g-1); r(f,0)" 10; } )
  0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765

  ${r}$ ${o}time bc0 's(0,10${r}^${o}4)' 10${r}^${o}2 '(17${r}^${o}n${r}-${o}16${r}^${o}n)${r}/${o}18${r}^${o}n'
  8.9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999997994
  3m30,299s

  ${r}$ ${o}n=10; bc0 n=\$n';print "Σ(2n+1,n=0.." , n , ") = " , s(0,n) , " = (" , n , "+1)² = " , (n+1)^2 , "\n"' 0 '2*n+1'
  Σ(2n+1,n=0..10) = 121 = (10+1)² = 121

  ${w}# soma de Simon Plouffe para aproximação de π${o}
  ${r}$ ${o}bc0 's(0,100)${r}-${o}3'        30 'n${r}*${o}2${r}^${o}n${r}*${o}f(n)${r}^${o}2${r}/${o}f(2${r}*${o}n)'
  3.141592653589793238462643381785
  ${r}$ ${o}bc0 's(0,100)${r}-${o}3${r}-${o}4${r}*${o}a(1)' 30 'n${r}*${o}2${r}^${o}n${r}*${o}f(n)${r}^${o}2${r}/${o}f(2${r}*${o}n)'
  -.000000000000000000000000001491

  ${w}# produto de Wallis para aproximação de π${o}
  ${r}$ ${o}time bc0 'print "p ≈ ",2*p(1,10^6),"\n"; print "π ≈ ",4*a(1),"\n"' 50 '4*n^2/(4*n^2-1)'
  p ≈ 3.14159186819212071459647668146464455064471802034566
  π ≈ 3.14159265358979323846264338327950288419716939937508
  0m7,393s

${w}Referências:${o}
  • http://gnu.org/software/bc/manual/html_mono/bc.html
  • http://phodd.net/gnu-bc/bcfaq.html
  • http://phodd.net/gnu-bc/code/funcs.bc
  • http://x-bc.sourceforge.net/extensions.bc
  • http://x-bc.sourceforge.net/scientific_constants.bc
  • http://wikipedia.org/wiki/Inverse_trigonometric_functions#Relationships_among_the_inverse_trigonometric_functions
EOT

    else

      grep -qEx '[0-9]+' <<< $1 && { sc=$1; op=$2; } || { t=$3; sc=$2; op=$1; }

      BC_LINE_LENGTH=0 bc -l << EOT

define i(n)
{
  s=scale  /* guarda o scale original */
  scale=0  /* resolve a próxima operação como um inteiro */
  i=n/1    /*  */
  scale=s  /* restaura o scale original */
  return i /* devolve o valor inteiro */
}

define o(n)
{
  i=i(n)
  if (n>0) return i else return i-1
}

define f(n)
{
  if(n<2)return 1
  return n*f(n-1)
}

define s(n,n1)
{
  if(n>n1)return 0
  return ${t:-0} + s(n+1,n1)
}

define p(n,n1)
{
  if(n>n1)return 1
  return ${t:-1} * p(n+1,n1)
}

define r(a,d)   /* http://askubuntu.com/a/666170 */
{
  s=scale       /* guarda o scale original */
  scale=0       /* resolve a próxima operação como um inteiro */
  t=(10^d)      /* a magnitude do scale resultante */
  scale=d+1     /* trabalha a partir de agora com um dígito a mais do que os 'd' dígitos desejados */
  h=0           /*  */
  if(a<0)h=-0.5 /*  */
  if(a>0)h=0.5  /*  */
  a=(a*t+h)     /* move o ponto decimal 'd' posições para a direita */
  scale=d       /* aplica o scale correspondente ao arredondamento desejado */
  a=a/t         /* move o ponto decimal 'd' posições para a esquerda mantendo apenas a quantidade 'd' de dígitos decimais */
  scale=s       /* restaura o scale original */
  return a      /* devolve o valor arredondado */
}

scale=${sc:-3}

$op

EOT

    fi

  }
# export BC_LINE_LENGTH=0



  dec2bas()
  {
    local D=$1 b=${2:-2} n=${3:-0} d B
    d=( {0..9} {a..z} {A..Z} @ _ ) # array com a sequência de dígitos conforme adotados pelo bash
    while (( $( bc <<< "$D>0||$n>0" ) == 1 ))
    do
      B=${d[$( bc <<< $D%$b )]}$B
      D=$( bc <<< $D/$b )
      ((n--))
    done
    echo $B
  }



  bas2dec()
  {
    local B=$1 b=${2:-2} d sub sup _o _r _y _b _w i n D=0 p v
      d=( {0..9} {a..z} {A..Z} @ _ ) # array com a sequência de dígitos conforme adotados pelo bash
    sub=( ₀ ₁ ₂ ₃ ₄ ₅ ₆ ₇ ₈ ₉ )
    sup=( ⁰ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ )
     _o="\e[0m" # código ANSI ao invés do comando tput para evitar comando+subshell
     _r="\e[1;31m"
     _y="\e[1;33m"
     _b="\e[1;34m"
     _w="\e[1;37m"
    (( $# == 0 || $# >  2 )) && { echo -e "\nConverte um número inteiro ${_b}B$_o numa base inteira 2≤${_y}b$_o≤64 (def: 2) para decimal.\n\nUso: $_w$FUNCNAME$_o <${_b}B$_o> [${_y}b$_o]\n" >&2; return 255; }
    [[ $* =~ [.,]         ]] && { echo    "Apenas valores inteiros são reconhecidos para o número e a base." >&2; return 3; }
    [[ $B =~ ^\ *-\ *     ]] && { echo    "Valores negativos não são reconhecidos para o número." >&2; return 2; }
    ((  b <  2 ||  b > 64 )) && { echo    "Base fora do intervalo reconhecido." >&2; return 1; }
    for ((n=0; n<${#B}; n++))
    {
      v=$(declare -p d | grep -oP "\[\K[^]]+(?=\]=\"${B:n:1})") # valor decimal do dígito na base B (conforme posicionado no array $d)
      (( v >= b )) && { echo -e "\n$_b${B:n:1}$_o ($v) é um dígito inválido na base $_y$b$_o." >&2; return 4; }
    # p[n]=$(( v*b**(${#B}-n-1) ))          # parcela do valor decimal de B relativa ao seu dígito n
      p[n]=$( bc <<< "$v*$b^(${#B}-$n-1)" ) # parcela do valor decimal de B relativa ao seu dígito n
      echo -en "$_b$v$_o×$_y$b$_r$( for i in $(grep -o . <<< $((${#B}-n-1))); { echo -n ${sup[$i]}; } )$_o "
      ((n<${#B}-1)) && echo -n '+ ' || echo =
    # ((D+=p[n]))                # soma das parcelas que no final conterá o valor decimal de B
      D=$( bc <<< "$D+${p[n]}" ) # soma das parcelas que no final conterá o valor decimal de B
    }
    p=${p[*]}
    p=${p// / +$'\n'}
    echo -e "$p ="
    echo -e "$_b$B$_y$( for i in $(grep -o . <<< $b); { echo -n ${sub[$i]}; } )$_o ="
    echo -e "$_w$D$_o₁₀"
  }
  # _@ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba9876543210₆₄ = 39392078757191557952714343944915560488424390381760365930566598193007564623154681709773698286566728461254900444831808₁₀
  # B=5i03FH
  # b=47
  # L=1000
  # time for ((i=0; i<L; i++)); { bas2dec $B $b   ; } &> /dev/null # 0m48,430s
  # time for ((i=0; i<L; i++)); { bas2dec $B $b   ; } &> /dev/null # 1m18,925s # usando bc
  # time for ((i=0; i<L; i++)); { b2dl    $B $b   ; } &> /dev/null # 0m20,215s
  # time for ((i=0; i<L; i++)); { b2dr    $B $b   ; } &> /dev/null # 0m26,387s
  # time for ((i=0; i<L; i++)); { echo $(($b#$B)) ; } &> /dev/null # 0m00,005s



  maxima0()
  {
    maxima --very-quiet --batch-string="$*;"
  }



  benshmark()
  {
    local LC_ALL= LC_NUMERIC=C L=$1 s i; shift
    for s
    {
      echo -n "$s  "
      time for ((i=0;i<L;i++)); { $s; } &> /dev/null
    }
  }



  sp()
  {

    [[ $1 ==  s ]] && { shift ; STATIC_TITLE=true ; return ; }
    [[ $1 ==  d ]] && { shift ; STATIC_TITLE=     ; return ; }

    [[ $1 == -h ]] && \
    {
      echo "
Set prompt.

Usage: $FUNCNAME [option] [string]

  Options
    -h       this help
    s        define title as static
    d        define title as dynamic (changes according working directory)
    def      set default prompt
    cond     set color prompt conditionaly
             (if ORIGIN_HN/ORIGIN_IP listed in file $SET_PROMPT)
    string   prefix prompt with identification string

  Depends on custom ${whi}environment variables${off}: TTY ORIGIN_HN ORIGIN_IP
                    ${whi}function${off}: is_from_set
"
      echo -n "Prompt defined as "
      [[ $STATIC_TITLE ]] && echo STATIC || echo DYNAMIC
      echo
      return
    }

  # local    hn='\h'
  # local    hn='$(hostname|cut -d. -f1)'
    local    hn='${HOSTNAME%%.*}'
  # local    tm='\D{%H:%M:%S}'
    local    tm='\t'

    local     a=$'\007'
    local     e=$'\033'
    local   bla="\[$e[1;30m\]"
    local   red="\[$e[1;31m\]"
    local   gre="\[$e[1;32m\]"
    local   yel="\[$e[1;33m\]"
    local   blu="\[$e[1;34m\]"
    local   mag="\[$e[1;35m\]"
    local   cya="\[$e[1;36m\]"
    local   whi="\[$e[1;37m\]"
    local   off="\[$e[0m\]"

    local  chrc=$red
    local numbc=$red
    local pathc=$whi
    local prefc=$gre

    local  ttyc=$cya
    local userc=$gre
    local hostc=$yel

    local label

  # local  ttyc=$off
  # local userc=$off
  # local hostc=$off

    local chr at ab fe title prompt
    if (( $(id -u) == 0 ))
    then
      chr="#"
       at=""
    else
      chr="$"
       at="$userc\u$chrc@"
    fi

    ab='['
    fe=']'

  # ab='|'
  # fe='|'

  # prompt="$numbc\! $ab$ttyc$TTY$chrc$fe$at$hostc$hn$chrc:$pathc\w$chrc$chr$off "
  # prompt="$numbc\! $at$ab$ttyc$TTY$chrc$fe$hostc$hn$chrc:$pathc\w$chrc$chr$off "

    [[ $1 == def ]] && { shift ; prompt="[\u@\h \W]\\$ " ; } || prompt="$numbc\! $ab$ttyc${TTY#*/}$chrc$fe$at$hostc$hn$chrc:$pathc\w$chrc$chr$off "

    if [[ $1 == t ]]
    then
      [[   $STATIC_TITLE ]] && return
      [[ ! $STATIC_TITLE ]] && { shift; title_only=true; } || { shift; title_only=; }
    else
      title_only=
    fi

    if [[ $# == 0 ]] || [[ $1 != cond ]] || [[ $1 == cond ]] && is_from_set $SET_PROMPT
    then
      [[ $1 == cond ]] && shift
      [[ $title_only && $PWD == $HOME ]] && label='~' || label=$*
      grep -q term <<< $TERM && { title="\[$e]0;$ab${TTY#*/}$fe "$( [[ $label ]] && echo "${label,,}" || echo "$hn:\w" )"$a\]"; }
      prompt="$title$prompt"
    fi

    label=$*
    [[ $label && ! $title_only ]] && prompt="$prefc$label$off $prompt"

    export PS1=$prompt

  }



  # show windows - lista sessões screen abertas por usuário
  sw()
  {

    local line head sed one

    line="  -----------------------------------------------------"
    head="  ##    sessão  início                  estado"

    # <http://superuser.com/a/203224>

    # BUG - no ubuntu do windows subsystem linux não está listando corretamente as sessões

    if [ $# = 0 ]
    then

      tty=${STY#*.}

      sed="

       #    s/\.[^.\t]+\t/\t/                       # remove '.<hostname>' do nome da sessão
            s/\t[^.]+\.([^.]+)(\.[^\t]+)?\t/\t\1\t/ # remove '<pid>.' e '.<hostname>' do nome da sessão

         \\\$s/^[^:]+://                            # remove o número da linha da última linha
            s/://                                   # remove a primeira ocorrência de ':' em cada linha
            /\t/s/^[^\t]+/\n  &/                    # nas linhas que tiverem tabulação (as mostrando sessões do screen), adiciona 2 espaços no início de cada linha, correspondentes aos 2 dígitos desejados no sequencial de cada sessão
            s/\n *(.{2})\t/  \1\t/                  # troca os 2 espaços iniciais de cada linha pela quantidade complementar ao número de digitos do sequencial da sessão screen, sobrando pelo menos 2 espaços em cada linha para o * indicador da sessão atual
       #    /\t/s/([^\t]+)\t/&     /                # nas linhas que tiverem tabulação, adiciona 5 espaços após o primeiro tab, correspondentes aos 5 dígitos desejados pid de cada sessão
            s/\t *(.{5})\./\t\1./                   # troca os 5 espaços após cada primeiro tab pela quantidade complementar ao número de digitos do pid da sessão screen

            s/\t\(/\t/g                             # remove caracter 'abre parênteses'
            s/\)(\t|$)/\1/g                         # remove caracter 'fecha parênteses'
            s/^ ( *[0-9]+\t *${tty:--})/*\1/        # coloca um * na primeira coluna da linha com a sessão atual do screen, se for o caso
            s/\.\r$//                               # remove o ponto final das linhas (basicamente a que informa o número de soquets do usuário)
            / S/s/^/  /                             # insere 2 espaços no início da linha informativa da quantidade de sessões
            s/ S/ s/                                # substitui S após um espaço por um minúsculo (basicamente a palavra Sockets, na linha informativa da quantidade deles)
            s/(No|[0-9]+) sockets? (found )?in //   #
            s/\t\([A-Z]/\L&/                        # no texto que sucede um '(', troca a primeira letra maiuscula pela correspondente minúscula, se for o caso
            s:\t([0-9]{2})[/-]([0-9]{2})[/-]([0-9]{4}):\t\3/\2/\1: # coloca a data de criação da sessão screen na forma yyyy/mm/dd

            1i\\\\\\\\
            1i\\$head
            1i\\$line
         \\\$i\\$line

          "

      one='(unset TZ; screen -wipe) | sort -t- -k2 -V | tail -n +2 | grep -n . | sed -r "'$sed'"'

      (
        ((UID==0)) && ls -1 /var/run/screen/ | cut -d- -f2 | grep -v "$USER" | while read user; do sudo -u $user bash -c "$one"; done
        bash -c "$one"
      ) | sed -r ":r;\$!{N;br};s/\n$head\n$line\n$line//g" # <http://stackoverflow.com/a/13733258>
    # ) | pcregrep -Mv "\n$head\n$line\n$line"             # <http://stackoverflow.com/a/2686705>

      echo
      echo "  sw            - lista sessões"
      echo "  sw <sessão>   - acessa sessão em modo exclusivo"
      echo "  sw <sessão> x - acessa sessão em modo compartilhado"
      echo "  sw <sessão> d - desatacha sessão remota"
      echo "  Control+A+D   - desatacha sessão ativa"
      echo "  sw d          - desatacha todas as sessões"
      echo

    else

      if [[ $1 == d ]]
      then #------------# desatacha todas as sessões
        while read session
        do
          screen -d $session &> /dev/null
        done < <(screen -wipe | cut -f2 | sed '1d;$d')
      elif [[ $2 == x ]]
      then #------------# acessa sessão no modo compartilhado (mantendo a janela original se estiver conectada)
        screen -x    $1
      elif [[ $2 == d ]]
      then #------------# desatacha a sessão
        screen -d    $1 &> /dev/null
      else #------------# acessa sessão no modo exclusivo (fechando a janela original se estiver conectada)
        screen -D -R $1
      fi

    fi

  }

  screen0() # <http://gnu.org/software/screen/manual/screen.html>
  {

  # cat $LOG | sed "s/^[\[H\^[\[2J//g" # remove códigos ANSI de limpeza de tela

    local key

    SLOG=$HOME/screen-%S-%n.log
  # SLOG=/var/log/screen-log/$(date +%Y%m%d-%H%M%S)-${TTY////_}-$ORIGIN_HN-$ORIGIN_IP
    S_RC="
           msgwait 0
           multiuser on
           shell -bash
           startup_message off
           term screen.xterm-xfree86                                # l /usr/share/terminfo/s/screen* (ncurses-term)
         # term screen-256color                                     # l /etc/terminfo/s/screen*
           attrcolor b '.I'                                         # allow bold colors - necessary for some reason
           termcapinfo xterm 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm' # tell screen how to set colors. AB = background, AF=foreground
           defbce on                                                # use current bg color for erased chars
           termcapinfo * ti=:te=                                    # <http://unix.stackexchange.com/a/224390>
         # termcapinfo * ti=vt340
         # defmode 0600
           logtstamp on
           logtstamp after 1
           logtstamp string \"\n[ TS %S %Y/%m/%d %D %0c:%s ]\n\"
           $(is_from_set $LOG_SCREEN && echo deflog  on    || echo deflog  off)
           $(is_from_set $LOG_SCREEN && echo logfile $SLOG || echo logfile /dev/null)
           logfile flush 0
         "
    T_RC=/tmp/screen-${TTY////-}.rc
    TOUT=30

    sw

    grep -q screen <<< $TERM || \
    { # inicia uma nova instância do screen apenas se já nao está em uma

      # BUG - pode ser necessário
      #         sudo mkdir      /run/screen
      #         sudo chmod 1777 /run/screen
      #         # /var/run -> /run

      # com algum parâmetro qualquer apresenta contagem regressiva no login para opção de uso do screen
      [[ $* ]] && \
      {
        echo -n "Iniciando nova sessão screen em ${TOUT}s; C para sessão sem screen, Q para abortar nova sessão, outra tecla para iniciar imediatamente:    ";
        for i in $(eval echo {$TOUT..1})
        do
          printf "\e[3D%2i " $i
          read -n 1 -t 1 -s key && break
        done
        key=$(echo $key | tr A-Z a-z)
      }

      case "$key" in

        c)
           echo -e "\e[D. Sem screen.\n"
           ;;

        q)
           echo -e "\e[D. Saindo.\n"
           exit
           ;;

        *)
           screen -ls | grep -qi detached \
           && screen -RR \
           || {
                echo "$S_RC" >| $T_RC
                LC_ALL=C screen -U -c $T_RC -S ${TTY////-} -R
                rm $T_RC
              }
           [[ $* ]] && exit
           ;;

      esac

    }

  }



  lwdcd()
  {
    # depends on custom environment variables: TTY
    # defines    custom environment variables: LWDFILE

    local WD

    [ "$1" != : ] \
    && WD=$1 \
    || {
         [ -s "$LWDFILE" ] && WD=$(grep -w "$TTY" "$LWDFILE" | cut -f3)
       }

    builtin cd "${WD:-$HOME}" \
    && {
         [ -s "$LWDFILE" ] \
         && {
              grep -wv "$TTY" "$LWDFILE" >| "$LWDFILE-$$"
              $(which -a mv | grep / | tail -n1) "$LWDFILE-$$" "$LWDFILE" # para contornar alias de segurança para mv
            }
         [ "$(readlink -f "$PWD")" != "$(readlink -f "$HOME")" ] && echo -e "$TTY\t$(/bin/date +'%Y/%m/%d %a %H:%M:%S %z')\t$PWD" >> "$LWDFILE"
       }

  }
  export LWDFILE="${LWDFILE:-$HOME/.lwd}"
  alias      lwd="[ -s \"$LWDFILE\" ] && sort -t/ -k2 -n \"$LWDFILE\" | sed -r \"s/^/  /;s:^ ( *\$TTY\t):*\1:\" | column0"
  alias       cd="lwdcd"



  abstract()
  {

    local   e=$'\033'
    local yel="$e[1;33m"
    local whi="$e[1;37m"
    local off="$e[0m"

    [[ $1 == -h ]] && \
    {
      echo "
Return a system general status abstract.

Usage: $FUNCNAME [option]

  Options
    -h     this help
    cond   return status conditionaly
           (if ORIGIN_HN listed in file $RUN_ABSTRACT)

  Depends on ${whi}custom environment variables${off}: TTY ORIGIN_IP ORIGIN_HN
             ${whi}functions${off}: is_from_set free0 ifconfig0 last0 crontab0 w0 df0 column0
             ${whi}aliases${off}: lwd
"
      return
    }

    is_from_set $RUN_ABSTRACT || return

    echo

    if (( $# == 0 )) || ( [[ $* == cond ]] && ! last0 | grep -w "$USER" | grep -v still | grep -qE "$(date +'%Y-(%b|%m)-%d')" )
    then

      count=$( echo -n $(LC_ALL=C lscpu 2> /dev/null | grep -Ei '^((cpu )?socket|core|thread)' | tac | sed -r 's/((cpu )?)([^(]+)\([^:]+: *(.+)/10#\4\u\3/i; $!s/$/ x/' ) )
      model=$( grep 'model name' /proc/cpuinfo 2> /dev/null | head -n1 | cut -d: -f2- | sed -r 's/ +/ /g' )

      echo -e "${whi}   HOSTNAME${off}  $HOSTNAME"
      echo -e "${whi}        CPU${off}  $(sed -r "s/10#//g; s/[0-9]+/$yel&$off/g" <<< $count) = $yel$(($(tr -d '[:alpha:]' <<< ${count//x/*})))${off}CPU -$model"
      echo -e "${whi}   LOAD_AVG${off}  $(echo $(paste -d ' ' <(LC_ALL=C uptime | sed -r "s/.*:([^:]+)/\1/; s/, /$off\n$yel/g; s/ /$yel/; \$s/$/$off/") <(echo -e "(1 min),\n(5 min),\n(15 min)")))"
      echo -e "${whi}     DISTRO${off}  $(. /etc/os-release 2> /dev/null && echo -e $NAME $(sed -r "s/[^ ]+/$yel&$off/" <<< $VERSION) || head -n1 /etc/issue 2> /dev/null | sed -r 's/ \\[a-z]//g')"
      echo -e "${whi}     KERNEL${off}  $(uname -vrms | sed -r "s/[0-9.]+/$yel&$off/")"
      echo -e "${whi}     UPTIME${off}  $(uptime0 | sed -r "s/[^-]+/$yel&$off/")"
      echo -e "${whi}       USER${off}  $USER"
      echo -e "${whi}        STY${off}  $STY"
      echo -e "${whi}        TTY${off}  $TTY"
      echo -e "${whi}       TERM${off}  $TERM"
      echo -e "${whi}      SHELL${off}  $SHELL"
      echo -e "${whi}    DISPLAY${off}  $DISPLAY"
      echo -e "${whi}BASH_SOURCE${off}  $BASH_SOURCE"
      echo -e "${whi}  PUBLIC_IP${off}  $PUBLIC_IP"
      echo -e "${whi}  ORIGIN_IP${off}  $ORIGIN_IP"
      echo -e "${whi}  ORIGIN_HN${off}  $ORIGIN_HN"
      echo -e "${whi} DEFAULT_GW${off}  $DEFAULT_GW"

      free0

    # locale | column -ts= | tr -d \"
    # echo

      echo -e " ${whi}#  NIC information${off}"
      ifconfig0 | cat -n | column0 | cut -c5-

      echo -e "\n${whi}Last Logged Users${off}"
      last0 -l

      echo -e "\n${whi}Crontab Tasks${off}"
      crontab0 -l

      echo
      w0

      echo
      df0 -h

      echo -e "\n${whi}Customized commands (inspect with 'type <custom_command>')${off}"
      echo " "$( (alias; typeset -F) | grep -Eo ' [^ 0]+0(=|$)' | tr -d = | sort )

      echo -e "\n${whi}Last Working Directories${off}"
      lwd || echo "none."

      echo -e "\n${whi}More hardware details:${off} inxi -Fv7"

    else
      echo "Run $whi$FUNCNAME$off to get a system general status abstract."
    fi

    echo

  }



  grep -q i <<< $- && \
  # if is an interactive shell and custom.sh not sourced yet
  {
    is_from_set $USE_SCREEN && screen0 login
    [[ ! $CUSTOM_SOURCED ]] && abstract cond # conditionaly show system abstract
    sp cond       # conditionaly set the prompt
    lwdcd :       # go to the last working directory used for the actual tty
    p
    export CUSTOM_SOURCED=true
  }



# EOF
