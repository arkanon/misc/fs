# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2020/09/11 (Fri) 21:49:43 -03
# 2018/04/06 (Fri) 09:53:53 -03



  set +H
  conf()
  {
    local d=i f=$1 p=$2 a=' ' v=$3 c='#|;|//' s k # debug file parameter assignment value comment sed command
    if [[ $# == 0 ]]
    then
      echo -e "\nUso: $FUNCNAME <arquivo> <parâmetro> [string de atribuição] <valor> [-d]\n"
      echo -e "     Se o valor do parâmetro já estiver no arquivo (mesmo comentado), altera-o (descomentando)"
      echo -e " OU, se não estiver, adiciona-o ao final do arquivo com o valor indicado.\n"
      echo -e "     Se o arquivo não existir, será criado com uma linha vazia e o parâmetro adicionado."
      echo -e "     Considera [$a] como string default de atribuição."
      echo -e "     Considera [$c] como string de comentário."
      echo -e "     Se o último parâmetro for -d (debug/diff), apenas mostra a diferença em relação ao original do que seria o resultado das alterações.\n"
      echo -e "   TODO"
      echo -e "     Tratar resultado com linhas repetidas"
      echo -e "     Implementar comentário de linha com parâmetro se o valor for determinada string (rem)"
      echo -e "     Parametrizar debug com resultado completo (-D)"
      echo -e "     Parametrizar string de comentário\n"
      return 255
    fi
    [[ ${@: -1} == -d ]] && { d=; set -- "${@:1:$(($#-1))}"; }
    [[ $#       ==  4 ]] && { a=$3; v=$4; }
    [[ -e $f          ]] || echo >> $f
    s="
        /\b$p\b/ { h ; s%^[ \t]*($c)*[ \t]*($p)[^[:alnum:]_]*$a.*%\2$a$v% }
        $ { x ; /^$/ { s%%$p$a$v% ; H } ; x }
        $ !N
        /^(.*$p$a).*\n\1.*$/ !P
        D
      "
    k="sed -r$d '$s' $f"
  # sed -n -r$d " s/^[^$c]*($p *$a *.*).*/$c\1/ ; $ a $p$a$v " $f
    if [[ $d ]]
    then
      . <(echo "$k")
    else
      echo -e "\n<  --  original\n>  --  modified\n"
      diff $f <(. <(echo "$k"))
      echo
    fi
  }
  set -H
  export -f conf



  clocomp()
  {
    eval "echo \"NTP $(ntpdate -q 172.23.1.1 | sed -r "1d; s/^(([^ ]+ ){2})([^ ]+).*(offset [^ ]+).*/\$(date -d '\1$(date +%Y) \3' +'%Y-%m-%d %H:%M:%S       %z') \4 s/")\""
    echo -n "RTC "; sudo hwclock -r
    echo -n " OS "; date +'%Y-%m-%d %H:%M:%S.%6N%z'
  }
  export -f clocomp



  dnsynt()
  {
    local resolv h f s d i t
    if [[ $# == 0 ]]
    then
      echo -e "\nDNs SYNc Time"
      echo -e "\nExecutado antes dos DNS's terem sincronizado suas bases entre si, mostrará o momento em que cada um se atualizar em relação ao IP atribuído dinamicamente a um hostname."
      echo -e "\nUso: $FUNCNAME <hostname>\n"
      return 255
    fi
    declare -A resolv=(  [1.1]=false  [1.10]=false  [0.94]=false  [0.95]=false  )
    h=$1
    f="%Y/%m/%d %H:%M:%S"
    s=$(date +"$f")
    echo -e "\n$s\n"
    while ! $(grep -oE '[^ ]+' <<< ${resolv[*]} | sort -u) # enquanto nem todos os dns's estiverem resolvendo o nome
    do
      for d in ${!resolv[@]} # para cada dns
      {
        if ! ${resolv[$d]} # se ele ainda não estiver resolvendo o nome
        then
          i=$(host $h 172.23.$d | grep address | cut -d\  -f4)
          t=$(date +"$f")
          if [[ $i ]] # se a tentativa de resolução foi bem sucedida
          then
            echo -e "$d\t$s\t$t\t$i"
            resolv[$d]=true
          fi
        fi
      }
      sleep 1
    done
    echo
  }
  export -f dnsynt



  kvip()
  {
    if [[ $# == 0 ]]
    then
      echo -e "\nUso: $FUNCNAME <if> <kvm vm>\n\nInterfaces\n"
      ifconfig \
      | grep -A1 flags \
      | sed -r '
                 s/^/  /
                 :a
                 /--/d
                 s/ +(broad|flags|tx|prefix).*//
                 s/(inet6?|ether) /\1\t/
                 s/  (netmask) /\t\1\t/
                 $!N
                 s/:\n +/\t/
                 ta
                 P
                 D
               ' \
      | sort -t$'\t' -k2 \
      | column -ts$'\t'
      echo -e "\nVMs\n"
      sudo virsh list --all | sed 's/^/ /'
      return 255
    fi
    local  if=$1
    local  vm=$2
    local mac=$(sudo virsh dumpxml $vm | grep "mac address" | cut -d "'" -f 2)
    local  ip=$(sudo arp-scan --interface $if -l | grep $mac | cut -f1)
    echo $ip
  }
  export -f kvip



  ms()
  {
  # SECONDS=0
  # task; task; ...
  # ms; echo
    eval printf %02i:%s $(date -ud @$SECONDS +'$[%s/60] %S')
  }
  export -f ms



  toip()
  {
    local e
    e='([0-9]{1,3}\.){3}[0-9]{1,3}'
    [[ $1 =~ $e ]] && echo $1 || host $1 | grep -oE "$e"
  }
  export -f toip



  tohn()
  {
    local l e
    l=$(host $1 | tail -n1)
    [[ $l =~ ^[0-9] ]] && e=' \K[a-z][^. ]+(?=\.)' || e='^[^.]+'
    grep -oP "$e" <<< $l
  }
  export -f tohn



# tovm()
# {
#   local l e
# }



# toid()
# {
#   local l e
# }



# tocn()
# {
#   local l e
# }



  debug()
  {
    export DB=${DB:-false} # DEBUG
    $DB && echo -e "DEBUG\t$1" >> /tmp/stderr
  }
  export -f debug



  run()
  {

    user=${user:-root}                        # user root dos nodos glusterfs para acesso por senha antes da chave ssh estar configurada
    sshk=${sshk:-$HOME/.ssh/sp-$USER/i-$USER} # chave (passphrase armazenada em ssh-agent) do user local que executará a função 'run'
    opts=${opts:--x -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=quiet -i $sshk -o User=$user} # parâmetros comuns a todas as chamadas ssh do procedimento

    if [[ $# == 0 ]]
    then
      echo -e "\nExecução em loop de (bloco de) comandos em uma lista de hosts ou em um host específico.\n"
      echo -e "Uso:"
      echo -e "                                $FUNCNAME [ip:|hostname:] <comando1> [-d]"
      echo -e "                                $FUNCNAME [ip:|hostname:] '<comando1>; <comando2>' [-d]"
      echo -e "                   <comando1> | $FUNCNAME [ip:|hostname:] 'cat > file; <comando2>' [-d]"
      echo -e "   type <função1> | tail -n+2 | $FUNCNAME [ip:|hostname:] '. <(cat); <função1>; <comando1>' [-d]\n"
      echo -e "   MUITA ATENÇÃO para o caracter ':' após o IP ou hostname em uma execução em host específico."
      echo -e "   Se o último parâmetro for -d (debug), apenas mostra o comando que seria executado em cada host.\n"
      echo -e "   RLIST - variável com a lista dos hosts acessados no loop"
      echo -e "   Ex: RLIST=\"172.17.17.17 10.10.10.1\""
      echo -e "       RLIST=\"172.17.{17.1,168.1}\"  # $(eval echo 172.17.{17.1,168.1})"
      echo -e "       RLIST=\"172.17.17.{10..15}\"   # $(eval echo 172.17.17.{10..15})\n"
      echo -e "Valor atual da lista (setado e expandido):"
      echo -e "   RLIST=\"$RLIST\""
      echo -e "   RLIST=\"$(eval echo $RLIST)\"\n"
      echo -e "Depende das variáveis (valores default):"
      echo -e "   user=\"$user\""
      echo -e "   sshk=\"$sshk\""
      echo -e "   opts=\"$opts\"\n"
      return 255
    fi

    [[ -t 0 ]] || stdin="$(< /dev/stdin)"

    local _list=

    if [[ $1 == *: ]]
    then
      _list=${1/:/}
      shift
    else
      _list=$(eval echo $RLIST)
    fi

    local   l=( "$@" )
    local  echo=

    if [[ ${@: -1} == -d ]]
    then
      echo=echo
      unset "l[${#l[@]}-1]"
    fi

    for host in $_list
    {
      echo -e "\n\n========== $host\n\n"
      { [[ $stdin ]] && echo "$stdin"; } | ssh $opts $host $echo ${l[*]}
    }

    echo -e "\n\n========== END\n\n"

  }
  export -f run



  powervm()
  {
    local vm pw username password domain o365pass vc_hn vc_un
    vm=$1
    pw=$2
    . $AD_C
    vc_hn=$(cat $HOME/.vcenter)
    vc_un=$username@$domain
    [[ $pw == on ]] && pw=start || pw=stop
    pwsh -command - << EOT > /dev/null
      connect-viserver -server $vc_hn -user $vc_un -password $password
      \$vm = get-vm -name $vm
      $pw-vm -vm $vm -confirm:\$false
EOT
  }
  export -f powervm



  vm_console_ip()
  {

    local username password domain o365pass vc_un upw

    if [[ $# != 1 ]]
    then
      echo "Usage: $FUNCNAME <vm name>"
      return 255
    fi

    . $AD_C
    vc_hn=$(cat $HOME/.vcenter)
    vc_un=$username@$domain
      upw=$password # senha do usuário a conectar no vCenter

     vmdb=/fs/src/git/ansible-security/_analise-hosts-ungp/..last/vci-2-ansb-5-23.txt
       on=$(grep poweredOn $vmdb | cut -f2,4,6)
       vm=$(grep -F "$1" <<< $on | cut -f1)
     vmhw=$(grep -F "$1" <<< $on | cut -f2)

    [[ $vm   ]] || { echo "Nenhuma VM relacionada" ; return 254 ; }
    [[ $vmhw ]] || { echo "Nenhum MAC relacionado" ; return 254 ; }
    if (( $(wc -l <<< $vm  ) > 1))
    then
      echo -e "\nMúltiplas VMs selecionadas:\n\n$(grep -F "$1" <<< $on | sort -t$'\t' -k2V | sed "s/^/   /" | column -ts$'\t')\n"
      return 254
    fi

    pxip=172.23.168.129 # proxy ip
     sed="s/(^[ \t]*#?|[ \t]+#.*$)//g;/^$/d" # remove comentários e linhas vazias

    ks="Set-VMKeystrokes -VMName '$vm' -Enter \$true -StringInput"
    ps="
         Import-Module /fs/bin/VMKeystrokes.ps1
         Connect-VIServer -Server $vc_hn -User $vc_un -Password $upw | Out-Null
                        $ks 'exit'
         Start-Sleep 2; $ks ''
         Start-Sleep 2; $ks 'root'
         Start-Sleep 2; $ks '_vmpw_'
         Start-Sleep 2; $ks 'ping -c1 $vc_hn' # coloca o mac da vm na arp do vc
         Start-Sleep 2; $ks 'exit'
         Start-Sleep 2; $ks ''
       "
    ps=$(sed -r "$sed" <<< $ps)

    echo -e "\n\n\n  vm='$vm'\nvmhw=$vmhw"
    for pwcl in o j b p
    {

      arp=$(ssh -x $vc_hn arp -an 2>&1 | grep -wF $vmhw || ssh -x $pxip arp -an 2>&1 | grep -wF $vmhw )
      sts=$?
      echo -e "\n\n\n arp='$arp'"
      (( sts == 0 )) && break

      echo -e "\n\n\npwcl=$pwcl"
      vmpw=$(cat $ADM_DIR/.${pwcl%-*}crd) # vm root password
      pwsh -Command - <<< ${ps/_vmpw_/$vmpw}
      sleep 10

    }
    echo -e "\n\n"

  }
  export -f vm_console_ip



  vm_shell_exec()
  {
    local username password domain o365pass vc_hn vc_un vc_pw vm_id vm_un vm_pw vm_cm vm_ar
  # git clone https://github.com/vmware/pyvmomi-community-samples
  # ln -s pyvmomi-community-samples/samples/tools
  # ln -s pyvmomi-community-samples/samples/execute_program_in_vm.py
  # vi execute_program_in_vm.py
  # # import re
  # # import time
    . $AD_C
    vc_hn=$(cat $HOME/.vcenter)
    vc_un=$username@$domain
    vc_pw=$password
    vm_id=$1
    vm_un=$ruser
    vm_pw=$ROOTPW
    vm_cm=$2
    vm_ar=$3
    python execute_program_in_vm.py -S -s$vc_hn -u$vc_un -p$vc_pw -v$vm_id -r$vm_un -w$vm_pw -l$vm_cm -f"$vm_ar; true"
  }
  export -f vm_shell_exec



  git_releases()
  {
     repo=$1
    idstr=$2
    ( [[ $idstr ]] && grep -qF $idstr <<< "$releases" ) || releases=$(lynx -dump -listonly -nonumbers -hiddenlinks=merge http://github.com/$repo/releases/latest | grep releases/download | sort -u)
    if [[ $# = 1 ]]
    then
      echo "$releases"
    else
      pkg=$(grep -F $idstr <<< "$releases" | tail -n1)
      echo "$pkg"
    fi
  }
  export -f git_releases



# EOF
